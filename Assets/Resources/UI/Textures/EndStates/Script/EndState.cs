﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndState : MonoBehaviour {

    [SerializeField]
    Sprite endStateDeath;
    [SerializeField]
    Sprite endStateWin;
    Image stateBG;

    void Awake()
    {
        stateBG = gameObject.GetComponent<Image>();
    }

	void OnEnable() {

        if (PlayerController.Instance.dead)
        {
            stateBG.sprite = endStateDeath;
            
        }
        else if (PlayerController.Instance.playerAcceptance)
        {
            stateBG.sprite = endStateWin;
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
            gameObject.transform.GetChild(1).gameObject.SetActive(false);
            StartCoroutine(secondsToMidnight());
        }
	
	}

    public void Replay()
    {
        PlayerController.Instance.revive = true;

    }


    IEnumerator secondsToMidnight()
    {

        float count = 0;
        Debug.Log(count);
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            count = count+Time.deltaTime;
            Debug.Log(count);
            if (count >= 3 || Input.anyKeyDown)
            {
                DestroyObject(Camera.main.gameObject, 0.005f);
                DestroyObject(GameObject.FindWithTag(PlayerController.Instance.PlayerTag).gameObject, 0.01f);
                Application.LoadLevel(0);
                yield break;
            }
        }

    }
	// Update is called once per frame
	void Update () {
	
	}

}
