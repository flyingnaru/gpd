﻿using UnityEngine;
using System.Collections;

public class DeckBuilding : MonoBehaviour {

    [SerializeField]
    GameObject mainMenu;
    [SerializeField]
    string gameManagerTag;

    // Use this for initialization
    void Start() {

    }

    #region Deck builder menu

    void IsDeckComplete()
    {

    }
    public void ToMainMenu()
    {
        gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void StartGame()
    {
        gameObject.SetActive(false);
        GameManager.ReceiveGameManager(GameObject.FindGameObjectWithTag(gameManagerTag));
        DontDestroyOnLoad(GameManager.LoadGameManager());
        Debug.Log(GameManager.LoadGameManager());
        Application.LoadLevel(1);
    }

    #endregion

    // Update is called once per frame
    void Update () {
	
	}
}
