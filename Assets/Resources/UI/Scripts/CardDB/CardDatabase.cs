﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardDatabase : Singleton<CardDatabase> {

    [SerializeField]
    private List<Card> cardLibrary;
    [SerializeField]
    private List<Card> playerABActions;
	// Use this for initialization
	void Awake () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}




    public Card GetLoot(CardTier tier)
    {
        Debug.Log(tier.ToString());
        List<Card> tempCards = new List<Card>();
        foreach (Card card in cardLibrary)
        {
            if (card.Tier == tier)
            {
                tempCards.Add(card);
            }
        }
        Debug.Log(tempCards.Count);
        return tempCards[Random.Range(0, tempCards.Count-1)];
    }

    public List<Card> CardLibrary
    {
        get
        {
            return cardLibrary;
        }
    }

    public List<Card> PlayerABactions
    {
        get
        {
            return playerABActions;
        }
    }


}
