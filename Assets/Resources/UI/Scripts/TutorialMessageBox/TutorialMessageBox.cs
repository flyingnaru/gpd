﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialMessageBox : MonoBehaviour {

    [SerializeField]
    Text message;
    List<string> messageBoxData;
    bool firstEncounter;
    bool firstLoot;
    bool firstEquiped;
	// Use this for initialization
	void Start () {

        GameStateController.TogglePause();
        PopulateTutorial();
        
	
	}
	
	// Update is called once per frame
	void Update () {

        EncounterFirstEnemy();
        FirstLootSpawned();

    }

    void PopulateTutorial()
    {
        messageBoxData = new List<string>();
        messageBoxData.Add("Click on a monster to attack it.\nPress 1 or 4 while pointing at a monster to use a card against it.");
        messageBoxData.Add("This monster dropped a card!\nPick it up and press D to see it in Lita's deck.");
    }

    public void CloseMessageBox()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        PlayerController.Instance.usingUI = false;
        GameStateController.TogglePause();
    }

    public void EncounterFirstEnemy()
    {
        if (PlayerController.Instance.inCombat && !firstEncounter)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            PlayerController.Instance.usingUI = true;
            message.text = messageBoxData[0];
            firstEncounter = true;
            GameStateController.TogglePause();
        }

    }

    public void FirstLootSpawned()
    {
        if (PlayerController.Instance.firstLootSpawned && !firstLoot)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            PlayerController.Instance.usingUI = true;
            message.text = messageBoxData[1];
            firstLoot = true;
            GameStateController.TogglePause();
        }
       
    }


}
