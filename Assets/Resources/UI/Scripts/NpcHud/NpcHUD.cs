﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class NpcHUD : MonoBehaviour {

    float sliderVal;
    [SerializeField]
    GameObject dmgContainer;
    [SerializeField]
    NpcCombat mob;
    [SerializeField]
    Slider mobHealth;
    [SerializeField]
    Text healthnumbers;
    [SerializeField]
    GameObject dmgprefab;

    void OnEnable()
    {
        gameObject.SetActive(true);
    }

    // Use this for initialization
    void Start () {

        init();

    }
	
	// Update is called once per frame
	void Update () {

        Hud();
        HealthBar();
        HealthNumbers();

    }


    public void displayTakenDmg(float damage)
    {
        GameObject newFloatingText = Instantiate(dmgprefab) as GameObject;
        RectTransform tempRect = newFloatingText.GetComponent<RectTransform>();
        tempRect.SetParent(dmgContainer.transform);
        tempRect.localPosition = dmgContainer.transform.localPosition;
        tempRect.localRotation = Quaternion.identity;
        tempRect.localScale = Vector3.one;
        newFloatingText.GetComponent<Text>().text = "-" + (int)damage;
        newFloatingText.GetComponent<Animator>().SetTrigger("Float");
        Destroy(newFloatingText,2);
    }

    void init()
    {
        sliderVal = mobHealth.value;
    }

    void Hud()
    {
        gameObject.transform.LookAt(Camera.main.gameObject.transform);
        if (mob.getHealth() == 0)
        {
            gameObject.SetActive(false);
        }
    }



    void HealthBar()
    {
        if (mob.getHealth() > 0)
        {
            mobHealth.value = sliderVal * (mob.getHealth() / mob.getMaxHealth());
        }


    }

    void HealthNumbers()
    {
        if (mob.getHealth() > 0)
        {
            healthnumbers.text = (int)mob.getHealth()+" / "+(int)mob.getMaxHealth();
        }
    }
}
