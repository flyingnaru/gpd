﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class PlayerCBLT : MonoBehaviour {

    [SerializeField]
    GameObject container;
    [SerializeField]
    GameObject dmgprefab;
    [SerializeField]
    GameObject healprefab;
    [SerializeField]
    GameObject lootprefab;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        CBLT();
	
	}

    void CBLT()
    {
        gameObject.transform.LookAt(Camera.main.gameObject.transform);
    }

    public void displayTakenDmg(float damage)
    {
        GameObject newFloatingText = Instantiate(dmgprefab) as GameObject;
        RectTransform tempRect = newFloatingText.GetComponent<RectTransform>();
        tempRect.SetParent(container.transform);
        tempRect.localPosition = container.transform.localPosition;
        tempRect.localRotation = Quaternion.identity;
        tempRect.localScale = Vector3.one;
        newFloatingText.GetComponent<Text>().text = "-" + (int)damage;
        newFloatingText.GetComponent<Animator>().SetTrigger("Float");
        Destroy(newFloatingText, 2);
    }

    public void displayReceivedHeal(float heal)
    {
        GameObject newFloatingText = Instantiate(healprefab) as GameObject;
        RectTransform tempRect = newFloatingText.GetComponent<RectTransform>();
        tempRect.SetParent(container.transform);
        tempRect.localPosition = container.transform.localPosition+new Vector3(1,0,0);
        tempRect.localRotation = Quaternion.identity;
        tempRect.localScale = Vector3.one;
        newFloatingText.GetComponent<Text>().text = "+" + (int)heal;
        newFloatingText.GetComponent<Animator>().SetTrigger("Float");
        Destroy(newFloatingText, 2);
    }

    public void displayLoot(bool looted,string lootName)
    {
        GameObject newFloatingText = Instantiate(lootprefab) as GameObject;
        RectTransform tempRect = newFloatingText.GetComponent<RectTransform>();
        tempRect.SetParent(container.transform);
        tempRect.localPosition = container.transform.localPosition;
        tempRect.localRotation = Quaternion.identity;
        tempRect.localScale = Vector3.one;
        if (looted)
        {
            newFloatingText.GetComponent<Text>().text = "Looted " + lootName;
        }
        else
        {
            newFloatingText.GetComponent<Text>().text = "Inventory is full!";
        }

        newFloatingText.GetComponent<Animator>().SetTrigger("Float");
        Destroy(newFloatingText, 2);
    }
}
