﻿using UnityEngine;
using System.Collections;

public class DialogueTrigger : MonoBehaviour {

    [SerializeField]
    DialogEvents triggeredEvent;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == PlayerController.Instance.PlayerBodyTag)
        {
            UIEventManager.Instance.TriggerEvent(triggeredEvent);
        }
    }
}
