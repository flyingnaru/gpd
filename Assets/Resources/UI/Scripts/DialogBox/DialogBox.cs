﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogBox : MonoBehaviour {
    [SerializeField]
    Sprite litaBox;
    [SerializeField]
    Sprite rapistBox;
    [SerializeField]
    Text diagText;
    Image diagBox;
    bool t;
    bool d1;
    bool d2;
    bool d3;
    bool d4;
    bool a1;
    bool a2;
    bool a3;
    bool a4;
    bool b1;
    bool b2;
    bool b3;
    bool b4;
    bool dp1;
    bool dp2;
    bool dp3;
    bool dp4;
    bool ac;
	// Use this for initialization
	void Start () {

        diagBox = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();
        UIEventManager.Instance.SubscribeEvent(DialogEvents.T, TutorialDialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.D1, Denial_D1Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.D2, Denial_D2Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.D3, Denial_D3Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.D4, Denial_D4Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.A1, Anger_A1Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.A2, Anger_A2Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.A3, Anger_A3Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.A4, Anger_A4Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.B1, Bargaining_B1Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.B2, Bargaining_B2Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.B3, Bargaining_B3Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.B4, Bargaining_B4Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.Dp1, Depression_Dp1Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.Dp2, Depression_Dp2Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.Dp3, Depression_Dp3Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.Dp4, Depression_Dp4Dialogue);
        UIEventManager.Instance.SubscribeEvent(DialogEvents.Ac, Acceptance_AcDialogue);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    #region Tutorial Diaglogue
    void TutorialDialogue()
    {
        if (!t)
        {
            StartCoroutine(TutorialDiag());
        }

    }

    IEnumerator TutorialDiag()
    {

        t = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = rapistBox;
        diagText.text = "Are you ready for today's session, Lita?";
        yield return new WaitForSeconds(2.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Yes. Let's get this over with.";
        yield return new WaitForSeconds(2);

        diagBox.gameObject.SetActive(false);
    }
    #endregion

    #region Denial Dialogue

    void Denial_D1Dialogue()
    {
        if (!d1)
        {
            StartCoroutine(Denial_D1());
        }

    }

    IEnumerator Denial_D1()
    {

        d1 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = rapistBox;
        diagText.text = "That is a beautiful set of cards you have, Lita.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = rapistBox;
        diagText.text = "What do they mean to you?";
        yield return new WaitForSeconds(2);

        diagBox.sprite = litaBox;
        diagText.text = "They are a gift from Mama.";
        yield return new WaitForSeconds(2);

        diagBox.sprite = litaBox;
        diagText.text = "It's silly, but I feel stronger with them.";
        yield return new WaitForSeconds(2.5f);

        diagBox.gameObject.SetActive(false);
    }

    void Denial_D2Dialogue()
    {
        if (!d2)
        {
            StartCoroutine(Denial_D2());
        }

    }

    
    IEnumerator Denial_D2()
    {

        d2 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "The forest is always so cold. Not like Papa.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "He's always happy or angry, always fired up.";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }

    void Denial_D3Dialogue()
    {
        if (!d3)
        {
            StartCoroutine(Denial_D3());
        }

    }

    IEnumerator Denial_D3()
    {

        d3 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "Even when Papa had trouble with work,";
        yield return new WaitForSeconds(2.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Mama would keep us strong and together.";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }

    void Denial_D4Dialogue()
    {
        if (!d4)
        {
            StartCoroutine(Denial_D4());
        }

    }

    IEnumerator Denial_D4()
    {

        d4 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "I can't keep pretending I'm ok when I'm not.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = rapistBox;
        diagText.text = "Yes. Don't be afraid to admit what you feel.";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }
    
    #endregion

    #region Anger Dialogue

    void Anger_A1Dialogue()
    {
        if (!a1)
        {
            StartCoroutine(Anger_A1());
        }

    }

    IEnumerator Anger_A1()
    {

        a1 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = rapistBox;
        diagText.text = "How are you feeling now, Lita?";
        yield return new WaitForSeconds(2.5f);

        diagBox.sprite = litaBox;
        diagText.text = "I feel angry! At myself, at him... ugh!";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }

    void Anger_A2Dialogue()
    {
        if (!a2)
        {
            StartCoroutine(Anger_A2());
        }

    }

    IEnumerator Anger_A2()
    {

        a2 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = rapistBox;
        diagText.text = "Was he angry very often?";
        yield return new WaitForSeconds(2);

        diagBox.sprite = litaBox;
        diagText.text = "Yes!... he used to be better though, more like himself.";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "That feels so long ago now.";
        yield return new WaitForSeconds(2);

        diagBox.gameObject.SetActive(false);
    }

    void Anger_A3Dialogue()
    {
        if (!a3)
        {
            StartCoroutine(Anger_A3());
        }

    }

    IEnumerator Anger_A3()
    {

        a3 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "He was so drunk, so violent that night.";
        yield return new WaitForSeconds(2.5f);

        diagBox.sprite = litaBox;
        diagText.text = "I heard Mama crying as I ran out into the forest. The bastard!";
        yield return new WaitForSeconds(4);

        diagBox.gameObject.SetActive(false);
    }

    void Anger_A4Dialogue()
    {
        if (!a4)
        {
            StartCoroutine(Anger_A4());
        }

    }

    IEnumerator Anger_A4()
    {

        a4 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "I shouldn't let my anger consume me, not like it did him.";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "It ruined him, and that destroyed my family.";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }

    #endregion

    #region Bargaining Dialogue

    void Bargaining_B1Dialogue()
    {
        if (!b1)
        {
            StartCoroutine(Bargaining_B1());
        }

    }

    IEnumerator Bargaining_B1()
    {

        b1 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "Was this all my fault? Could I...";
        yield return new WaitForSeconds(2.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Should I have done something different?";
        yield return new WaitForSeconds(3);

        diagBox.gameObject.SetActive(false);
    }

    void Bargaining_B2Dialogue()
    {
        if (!b2)
        {
            StartCoroutine(Bargaining_B2());
        }

    }

    IEnumerator Bargaining_B2()
    {

        b2 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "I still don't understand why he chased me that night...";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Was it something I did wrong?";
        yield return new WaitForSeconds(2);

        diagBox.gameObject.SetActive(false);
    }

    void Bargaining_B3Dialogue()
    {
        if (!b3)
        {
            StartCoroutine(Bargaining_B3());
        }

    }

    IEnumerator Bargaining_B3()
    {

        b3 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "I begged him to stop as he grabbed me.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "I pleaded as he tore off my clothes.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "He still violated me that night.";
        yield return new WaitForSeconds(2.5f);

        diagBox.gameObject.SetActive(false);
    }

    void Bargaining_B4Dialogue()
    {
        if (!b4)
        {
            StartCoroutine(Bargaining_B4());
        }

    }

    IEnumerator Bargaining_B4()
    {

        b4 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = rapistBox;
        diagText.text = "The past cannot be changed, and that...";
        yield return new WaitForSeconds(3);

        diagBox.sprite = rapistBox;
        diagText.text = "...is a helpful but hard truth to accept.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "Yes. I see that now.";
        yield return new WaitForSeconds(2);

        diagBox.gameObject.SetActive(false);
    }

    #endregion

    #region Depression Dialogue

    void Depression_Dp1Dialogue()
    {
        if (!dp1)
        {
            StartCoroutine(Depression_Dp1());
        }

    }

    IEnumerator Depression_Dp1()
    {

        dp1 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "There's nothing more I can do. I feel so... hopeless.";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = rapistBox;
        diagText.text = "That's not entirely true. You can still help yourself.";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "What's the point?";
        yield return new WaitForSeconds(2);

        diagBox.gameObject.SetActive(false);
    }

    void Depression_Dp2Dialogue()
    {
        if (!dp2)
        {
            StartCoroutine(Depression_Dp2());
        }

    }

    IEnumerator Depression_Dp2()
    {

        dp2 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "I saw the flame in him die as he crawled back towards me,";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Slipping on the blood oozing out from his back.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "The kitchen knife was still there.";
        yield return new WaitForSeconds(2.5f);

        diagBox.gameObject.SetActive(false);
    }

    void Depression_Dp3Dialogue()
    {
        if (!dp3)
        {
            StartCoroutine(Depression_Dp3());
        }

    }

    IEnumerator Depression_Dp3()
    {

        dp3 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "Even with her neck broken,";
        yield return new WaitForSeconds(2);

        diagBox.sprite = litaBox;
        diagText.text = "Mama looked so strong and calm as she lay there.";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "Why did she have to die?!";
        yield return new WaitForSeconds(2.5f);

        diagBox.gameObject.SetActive(false);
    }

    void Depression_Dp4Dialogue()
    {
        if (!dp4)
        {
            StartCoroutine(Depression_Dp4());
        }

    }

    IEnumerator Depression_Dp4()
    {

        dp4 = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "Mama wouldn't want me to suffer inside.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "I must be better than this.";
        yield return new WaitForSeconds(2.5f);

        diagBox.gameObject.SetActive(false);
    }

    #endregion

    #region Acceptance Dialogue

    void Acceptance_AcDialogue()
    {
        if (!ac)
        {
            StartCoroutine(Acceptance_Ac());
        }

    }

    IEnumerator Acceptance_Ac()
    {

        ac = true;
        diagBox.gameObject.SetActive(true);

        diagBox.sprite = litaBox;
        diagText.text = "The last things I remember are Mama stabbing Papa,";
        yield return new WaitForSeconds(3.5f);

        diagBox.sprite = litaBox;
        diagText.text = "...then him strangling her before dying himself.";
        yield return new WaitForSeconds(3);

        diagBox.sprite = litaBox;
        diagText.text = "I must have called the police, but I don't remember doing it.";
        yield return new WaitForSeconds(3.5f);

        diagBox.gameObject.SetActive(false);
    }

    #endregion
}
