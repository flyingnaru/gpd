﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CardContainer : MonoBehaviour {

    [SerializeField]
    private GameObject inventorySlots;
    [SerializeField]
    private GameObject actionBarSlots;
    private GameObject firstSelectedSlot;
    private GameObject secondSelectedSlot;
    private Card originCard;
    private Card destinationCard;
    private bool firstLoaded;
    private bool secondLoaded;
    [SerializeField]
    private bool ActionBar;
    private CardSlot currentlySelectedSlot;
    private Sprite currentlySelectedCard;
    private string currentlySelectedCardName;
    private string currentlySelectedCardDesc;
    private string currentlySelectedCardUses;
    private static bool deckComplete;
    private bool deckLoaded;
    private bool inventoryOpen;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
    
    }

    #region Init Functions

    #endregion

    #region CheckFunctions

    public bool InventoryIsFull() 
    {
           
            int freeSlots=0;

            for (int i = 0; i < inventorySlots.transform.childCount; i++)
            {
                if (inventorySlots.transform.GetChild(i).GetComponent<CardSlot>().GetCardInSlot() != null)
                {
                    freeSlots++;
                }
            }
            if (freeSlots == inventorySlots.transform.childCount)
            {
                return true;
            }
            else
            {
                return false;
            }
                
        }
    public void LoadInABSlot(CardType action,Card card)
    {
        switch (action)
        {
        
            case CardType.Damage:
                {
                    CardSlot abS1 = actionBarSlots.transform.GetChild(0).GetComponent<CardSlot>();
                    if (abS1.GetCardInSlot().Tier != CardTier.Basic)
                    {
                        LoadInInventorySlot(abS1.GetCardInSlot());
                    }
                    abS1.StoreCard(card);
                    break;
                }
            case CardType.Heal:
                {
                    CardSlot abS2 = actionBarSlots.transform.GetChild(1).GetComponent<CardSlot>();
                    if (abS2.GetCardInSlot().Tier != CardTier.Basic)
                    {
                        LoadInInventorySlot(abS2.GetCardInSlot());
                    }
                    abS2.StoreCard(card);
                    break;
                }
            case CardType.Buff:
                {
                    CardSlot abS3 = actionBarSlots.transform.GetChild(2).GetComponent<CardSlot>();
                    if (abS3.GetCardInSlot().Tier != CardTier.Basic)
                    {
                        LoadInInventorySlot(abS3.GetCardInSlot());
                    }
                    abS3.StoreCard(card);
                    break;
                }
            case CardType.Utility:
                {
                    CardSlot abS4 = actionBarSlots.transform.GetChild(3).GetComponent<CardSlot>();
                    if (abS4.GetCardInSlot().Tier != CardTier.Basic)
                    {
                        LoadInInventorySlot(abS4.GetCardInSlot());
                    }
                    abS4.StoreCard(card);
                    break;
                }
        }
    }
    public bool LoadInInventorySlot(Card card)
    {
        bool loaded=false;
        if (!InventoryIsFull())
        {
            for (int i = 0; i < inventorySlots.transform.childCount; i++)
            {
                CardSlot slot = inventorySlots.transform.GetChild(i).GetComponent<CardSlot>();
                if (slot.GetCardInSlot() == null)
                {
                    slot.StoreCard(card);
                    loaded = true;
                    break;
                }
                loaded = false;
            }

            if (loaded)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
       
        
      
    }

#endregion

#region Usage Functions
    public void ExchangeMechanicRightClick(CardSlot selected)
    {
        if (inventoryOpen)
        {
            switch (selected.SlotType)
            {
                case SlotType.InventorySlot:
                    {
                        if (selected.GetCardInSlot() != null)
                        {
                            Card selectedCard = selected.GetCardInSlot();
                            selected.Discard();

                            Debug.Log(selectedCard.Type);
                            switch (selectedCard.Type)
                            {

                                case CardType.Damage:
                                    {
                                        Debug.Log(selectedCard.GetName());
                                        LoadInABSlot(CardType.Damage, selectedCard);

                                        break;
                                    }
                                case CardType.Heal:
                                    {
                                        LoadInABSlot(CardType.Heal, selectedCard);
                                        break;
                                    }
                                case CardType.Buff:
                                    {
                                        LoadInABSlot(CardType.Buff, selectedCard);
                                        break;
                                    }
                                case CardType.Utility:
                                    {
                                        LoadInABSlot(CardType.Utility, selectedCard);
                                        break;
                                    }
                            }
                        }
                        break;
                    }
                case SlotType.ActionBarSlot:
                    {
                        if (selected.GetCardInSlot().Tier != CardTier.Basic)
                        {
                            if (LoadInInventorySlot(selected.GetCardInSlot()))
                            {
                                selected.Discard();
                                selected.LoadBasicAbility();
                            }

                        }

                        break;
                    }
            }
        }

    }
    #endregion

    #region CurrentCard
    public void LoadCard(CardSlot cardSlot)
    {
        if (cardSlot.GetCardInSlot() != null)
        {
            if (currentlySelectedSlot != cardSlot || currentlySelectedSlot == null)
            {
                currentlySelectedSlot = cardSlot;
                if (cardSlot.GetCardInSlot() != null)
                {
                    currentlySelectedCard = cardSlot.GetCardInSlot().cardImg;
                    currentlySelectedCardName = cardSlot.GetCardInSlot().GetName();
                    currentlySelectedCardDesc = cardSlot.GetCardInSlot().GetDescription();
                    currentlySelectedCardUses = cardSlot.GetCardInSlot().CardUses.ToString();
                }

            }
        }

    }
    public void UnLoadCard()
    {
        if (currentlySelectedCard != null)
        {
            currentlySelectedSlot = null;
            currentlySelectedCard = null;
            currentlySelectedCardName = string.Empty;
            currentlySelectedCardDesc = string.Empty;

        }

    }
    #endregion

    #region Getter/Setter
    public Sprite CurrentCard
    {
        get
        {
            return currentlySelectedCard;
        }
    }

    public string CurrentCardName
    {
        get
        {
            return currentlySelectedCardName;
        }
    }

    public bool InventoryOpened
    {
        get
        {
            return inventoryOpen;
        }

        set
        {
            inventoryOpen = value;
        }

    }

    public string CurrentlySelectedCardDesc
    {
        get
        {
            return currentlySelectedCardDesc;
        }
    }

    public string CurrentlySelectedCardUses
    {
        get
        {
            return currentlySelectedCardUses;
        }
    }
    #endregion 
}
