﻿using UnityEngine;
//This is the Card Base class, that can be used to create different cards within the game.

public enum CardType
{
    None,
    Damage,
    Heal,
    Buff,
    Utility
}

public enum CardTarget
{
    Self,
    Enemy
}

public enum CardTier
{
    Basic,
    Tutorial,
    Tier1,
    Tier2,
    Tier3,
    Tier4
}

public enum CardActions
{
    None,
    DirectDamage,
    DirectHeal,
    DoT,
    HoT,
    Stealth,
    Stun,
    DamageMod,
    DamageReductionMod

}

[System.Serializable]
public class Card
{
    public NpcCombat _playerTarget;
    public PlayerCombat _player;

    [SerializeField]
    public Sprite cardImg;
    [SerializeField]
    string cardName;
    [Range(1, 100)]
    [SerializeField]
    int cardCost=1;
    [SerializeField]
    CardType type;
    [SerializeField]
    CardTarget target;
    [SerializeField]
    CardTier tier;

    [SerializeField]
    public CardActions firstAction;
    [SerializeField]
    [Range(-100, 100)]
    public float firstValue = 1.0f;
    [SerializeField]
    [Range(1, 10)]
    public float firstMultiplier = 1.0f;
    [SerializeField]
    public int firstTime;

    [SerializeField]
    public CardActions secondAction;
    [SerializeField]
    [Range(-100, 100)]
    public float secondValue = 1.0f;
    [SerializeField]
    [Range(1, 10)]
    public float secondMultiplier = 1.0f;
    [SerializeField]
    public int secondTime;

    [SerializeField]
    public CardActions thirdAction;
    [SerializeField]
    [Range(-100, 100)]
    public float thirdValue = 1.0f;
    [SerializeField]
    [Range(1, 10)]
    public float thirdMultiplier = 1.0f;
    [SerializeField]
    public int thirdTime;

    [SerializeField]
    int cardUses;
    [SerializeField]
    string cardDescription;


    public Card(Card clone)
    {
        _player = clone._player;
        _playerTarget = clone._playerTarget;
        cardImg = clone.cardImg;
        cardName = clone.GetName();
        cardCost = clone.CardCost;
        type = clone.Type;
        target = clone.Target;
        tier = clone.Tier;
        firstAction = clone.firstAction;
        firstMultiplier = clone.firstMultiplier;
        firstValue = clone.firstValue;
        firstTime = clone.firstTime;
        secondAction = clone.secondAction;
        secondMultiplier = clone.secondMultiplier;
        secondValue = clone.secondValue;
        secondTime = clone.secondTime;
        thirdAction = clone.thirdAction;
        thirdMultiplier = clone.thirdMultiplier;
        thirdValue = clone.thirdValue;
        thirdTime = clone.thirdTime;
        cardUses = clone.CardUses;
        cardDescription = clone.GetDescription();

        
    }   

    public void UseCard(GameObject entity)
    {
        switch (target)
        {
            case CardTarget.Enemy:
                {
                    _playerTarget = entity.GetComponent<NpcCombat>();
                    _player = GameObject.FindGameObjectWithTag(PlayerController.Instance.PlayerTag).GetComponent<PlayerCombat>();
                    break;
                }
            case CardTarget.Self:
                {
                    _player = GameObject.FindGameObjectWithTag(PlayerController.Instance.PlayerTag).GetComponent<PlayerCombat>();
                    break;
                }

        }

        switch (type)
        {

            //HANDLE COMBAT ACTIONS
            #region Damage Actions
            case CardType.Damage:
                {
                    switch (target)
                    {
                        case CardTarget.Enemy:
                            {
                                switch (firstAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectDamage:
                                        {
                                            _playerTarget.CastedReceiveDamage(firstValue * firstMultiplier);
                                            break;
                                        }

                                    case CardActions.DoT:
                                        {
                                            _playerTarget.DoTNpc(firstValue*firstMultiplier,firstTime);
                                            //call action
                                            break;
                                        }

                                }

                                switch (secondAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectDamage:
                                        {
                                            _playerTarget.CastedReceiveDamage(secondValue * secondMultiplier);
                                            //call action
                                            break;
                                        }

                                    case CardActions.DoT:
                                        {
                                            _playerTarget.DoTNpc(secondValue * secondMultiplier, secondTime);
                                            //call action
                                            //Maybe Bug
                                            break;
                                        }

                                    case CardActions.DirectHeal:
                                        {
                                            _player.Heal(secondValue);
                                            break;
                                        }


                                }

                                switch (thirdAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectDamage:
                                        {
                                            _playerTarget.CastedReceiveDamage(thirdValue * thirdMultiplier);
                                            //call action
                                            break;
                                        }

                                    case CardActions.DoT:
                                        {
                                            _playerTarget.DoTNpc(thirdValue * thirdMultiplier, thirdTime);
                                            //call action
                                            break;
                                        }
                                }
                                break;
                            }
                        
                    }
                    break;
                }
            #endregion
            #region Heal Actions
            case CardType.Heal:
                {
                    switch (target)
                    {
                        case CardTarget.Self:
                            {
                                switch (firstAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectHeal:
                                        {
                                            _player.CastedHeal(firstValue * firstMultiplier);
                                            Debug.Log("CALL");
                                            //call action
                                            break;
                                        }

                                    case CardActions.HoT:
                                        {
                                            _player.HoTPlayer(firstValue * firstMultiplier, firstTime);
                                            //call action
                                            break;
                                        }
                                }

                                switch (secondAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectHeal:
                                        {
                                            _player.CastedHeal(secondValue * secondMultiplier);
                                            //call action
                                            break;
                                        }

                                    case CardActions.HoT:
                                        {
                                            _player.HoTPlayer(secondValue * secondMultiplier, secondTime);
                                            //call action
                                            break;
                                        }
                                }

                                switch (thirdAction)
                                {
                                    case CardActions.None:
                                        {
                                            break;
                                        }
                                    case CardActions.DirectHeal:
                                        {
                                            _player.CastedHeal(thirdValue * thirdMultiplier);
                                            //call action
                                            break;
                                        }

                                    case CardActions.HoT:
                                        {
                                            _player.HoTPlayer(thirdValue * thirdMultiplier, thirdTime);
                                            //call action
                                            break;
                                        }
                                }

                                break;
                            }
                    }
                    break;
                }
            #endregion
            #region Buff
            case CardType.Buff:
                {

                    switch (target)
                    {
                        case CardTarget.Self:
                            {
                                switch (firstAction)
                                {
                                    case CardActions.DamageMod:
                                        {
                                            _player.PlayerDMGMod(firstValue * firstMultiplier, firstTime);
                                            break;
                                        }
                                    case CardActions.DamageReductionMod:
                                        {
                                            _player.PlayerDRMod(firstValue * firstMultiplier, firstTime);
                                            break;
                                        }
                                }

                                switch (secondAction)
                                {
                                    case CardActions.DamageMod:
                                        {
                                            _player.PlayerDMGMod(secondValue * secondMultiplier, secondTime);
                                            break;
                                        }
                                    case CardActions.DamageReductionMod:
                                        {
                                            _player.PlayerDRMod(secondValue * secondMultiplier, secondTime);
                                            break;
                                        }
                                }

                                switch (thirdAction)
                                {
                                    case CardActions.DamageMod:
                                        {
                                            _player.PlayerDMGMod(thirdValue * thirdMultiplier, thirdTime);
                                            break;
                                        }
                                    case CardActions.DamageReductionMod:
                                        {
                                            _player.PlayerDRMod(thirdValue * thirdMultiplier, thirdTime);
                                            break;
                                        }
                                }
                                break;
                            }
                    }
                    break;

                }
            #endregion
            #region Utility
            //HANDLE TOOLS
            case CardType.Utility:
                {
                    switch (target)
                    {

                        case CardTarget.Self:
                            {
                                switch (firstAction)
                                {
                                    case CardActions.Stealth:
                                        {
                                            _player.Stealth(firstTime);
                                            break;
                                        }
                                }
                                break;
                            }
                        case CardTarget.Enemy:
                            {
                                switch (firstAction)
                                {
                                    case CardActions.Stun:
                                        {
                                            _playerTarget.StunNpc(firstTime);
                                            break;
                                        }
                                }

                                break;
                            }
                    }
                    break;
                }
                #endregion
        }
        cardUses--;
    }

    public string GetName()
    {
        return cardName;

    }

    public string GetDescription()
    {
        return cardDescription;

    }

    public int CardCost
    {
        get
        {
            return cardCost;
        }
    }

    public CardTier Tier
    {
        get
        {
            return tier;
        }
    }

    public CardTarget Target
    {
        get
        {
            return target;
        }
    }

    public CardType Type
    {
        get
        {
            return type;
        }
    }

    public int CardUses
    {
        get
        {
            return cardUses;
        }
    }
}
