﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour {

    [SerializeField]
    GameObject newGameBtn;

    [SerializeField]
    GameObject quitGameBtn;

    [SerializeField]
    GameObject teamBtn;

    [SerializeField]
    GameObject teamBackBtn;

    [SerializeField]
    GameObject overlay;
    Text creditsText;
    bool coroutinebreaker;
    string[] texts;
    // Use this for initialization
    void Start ()
    {
        creditsText = overlay.transform.GetChild(0).gameObject.GetComponent<Text>();
        texts = new string[17];
        texts[0] = "ART AND ANIMATION\n\nTanya Tasheva";
        texts[1] = "AUDIO\n\nPraveen Namasivayam";
        texts[2] = "DESIGN\n\nMiruna Vozaru";
        texts[3] = "PROGRAMING\n\nBusnita Alexandru";
        texts[4] = "STORY\n\nPraveen Namasivayam";
        texts[5] = "EXTERNAL SOURCES\n\nBase Sfx";
        texts[6] = "SOURCE\n\nfreesound.org";
        texts[7] = "CREATORS\n\ntigersound abyssmal skiersailor\nmichel88 jankoehl taylorsyoung cameronmusic\nmokebomb99 slave2thelight under7dude";
        texts[8] = "mrpokephile alex-vsi-tv huminaatio\nabsent1010 soykevin agaxly\nannabloom timbre themattfreeman";
        texts[9] = "ctcollab f4ngy hoerspielwerkstatt\nrempen coral-island-studios manuts";
        texts[10] = "EXTERNAL SOURCES\n\nBase Art";
        texts[11] = "SOURCE\n\nUnity3D Asset Store";
        texts[12] = "TERRAIN TEXTURE ASSETS\n\nBrian Blinks";
        texts[13] = "BUSH ASSETS\n\nNobiax / Yughues";
        texts[14] = "EXTERNAL SOURCES\n\nGraphics programing";
        texts[15] = "SOURCE\n\nUnity3D Asset Store";
        texts[16] = "PARTICLE SYSTEMS SHADERS\n\nMichael Kremmel";

    }

    #region MainMenu functions
    public void Quit()
    {
        
        Application.Quit();
    }

    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Back()
    {
        coroutinebreaker = true;
        overlay.SetActive(false);
        teamBackBtn.SetActive(false);
        newGameBtn.SetActive(true);
        quitGameBtn.SetActive(true);
        
    }

    public void Credits()
    {
        coroutinebreaker = false;
        overlay.SetActive(true);
        teamBackBtn.SetActive(true);
        newGameBtn.SetActive(false);
        quitGameBtn.SetActive(false);
        StartCoroutine(GameCredits());

    }

    IEnumerator GameCredits()
    {
        int count = 0;
        while (!coroutinebreaker && count<texts.Length)
        {
            creditsText.text = texts[count];
            yield return new WaitForSeconds(2);
            count++;
        }
        yield return new WaitForSeconds(1);
        creditsText.text = "THANK YOU FOR PLAYING!";
        yield return new WaitForSeconds(2.5f);
        creditsText.text = string.Empty;
        yield return new WaitForSeconds(1);
        Back();
        yield break;
    }
    #endregion
}
