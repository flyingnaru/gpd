﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardInventory : MonoBehaviour {

    [SerializeField]
    GameObject inventory;
    [SerializeField]
    GameObject tips;
    CardContainer container;
	// Use this for initialization
	void Start () {

        container = gameObject.GetComponent<CardContainer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.D) )
        {
            if (!container.InventoryOpened)
            {
                inventory.SetActive(true);
                container.InventoryOpened = true;
            }
            else
            {
                inventory.SetActive(false);
                container.InventoryOpened = false;
            }

        }
	
	}

    public void OpenCloseInventory()
    {
        if (!container.InventoryOpened)
        {
            inventory.SetActive(true);
            container.InventoryOpened = true;
        }
        else
        {
            inventory.SetActive(false);
            container.InventoryOpened = false;
        }
    }

    public void ShowHideTips(Text tip)
    {
        if (tips.activeSelf)
        {
            tips.SetActive(false);
            tip.text = "OFF";
        }
        else
        {
            tips.SetActive(true);
            tip.text = "ON";
        }
    }
}
