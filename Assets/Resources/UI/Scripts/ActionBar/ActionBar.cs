﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ActionBar : MonoBehaviour
{

    [SerializeField]
    Texture2D pointerBurnTeture;
    [SerializeField]
    Color disabledCard;
    [SerializeField]
    private Color enabledCard;
    [SerializeField]
    private Text cardNumberDisplay;
    [SerializeField]
    private GameObject actionBarSlots;
    [SerializeField]
    private GameObject toolTip;
    private Text toolTipText;
    private RectTransform toolTipRect;
    private List<Image> quickSlotContainers;
    private List<CardSlot> quickSlots;
    private PlayerCombat player;
    private NpcCombat currentTarget;
    private Canvas myCanvas;
    // Use this for initialization
    void Start()
    {

        InitActionBar();

    }

    // Update is called once per frame
    void Update()
    {

        checkQuickslots();
        ActionBarInput();
        CurrentCards();

    }

    #region Init

    void InitActionBar()
    {
        player = GameObject.FindWithTag(PlayerController.Instance.PlayerTag).GetComponent<PlayerCombat>();
        quickSlots = new List<CardSlot>();
        quickSlotContainers = new List<Image>();
        //   burnQuickslotContainers = new List<Button>();
        for (int i = 0; i < actionBarSlots.transform.childCount; i++)
        {
            quickSlotContainers.Add(actionBarSlots.transform.GetChild(i).GetComponent<Image>());
            quickSlots.Add(actionBarSlots.transform.GetChild(i).GetComponent<CardSlot>());
            // burnQuickslotContainers.Add(actionBarSlots.transform.GetChild(i).GetComponent<Button>());
        }

        toolTipRect = toolTip.GetComponent<RectTransform>();
        toolTipText = toolTip.transform.GetChild(0).GetComponent<Text>();
        myCanvas = toolTip.transform.parent.GetComponent<Canvas>();
    }

    #endregion
   
    void CurrentCards()
    {
        cardNumberDisplay.text = "";// CardDatabase.Instance.PlayerInventory.Count.ToString();
    }

    #region Use Quickslots
    void checkQuickslots()
    {
        for (int i = 0; i < quickSlots.Count; i++)
        {
            if (quickSlots[i].GetCardInSlot() != null)
            {
                Card tempCard = quickSlots[i].GetCardInSlot();
                switch (tempCard.Target)
                {
                    case CardTarget.Enemy:
                        {
                            if (PlayerController.Instance.PlayerTarget != null)
                            {
                                if (PlayerController.Instance.PlayerTarget.tag == NpcHostileController.Instance.NpcTag || PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerBosstag)
                                {
                                    if (currentTarget == null)
                                    {
                                        currentTarget = PlayerController.Instance.PlayerTarget.GetComponent<NpcCombat>();
                                    }
                                    else if (currentTarget.GetInstanceID() != PlayerController.Instance.PlayerTarget.GetComponent<NpcCombat>().GetInstanceID())
                                    {
                                        currentTarget = PlayerController.Instance.PlayerTarget.GetComponent<NpcCombat>();
                                    }
                                    else
                                    {
										quickSlotContainers[i].color = enabledCard;
                                       
                                    }
                                }
                                else
                                {
                                    quickSlotContainers[i].color = disabledCard;
                                }
                            }
                            else
                            {
                                quickSlotContainers[i].color = disabledCard;
                            }
                            break;
                        }

                    case CardTarget.Self:
                        {
                            quickSlotContainers[i].color = enabledCard;
                            break;
                        }

                }
            }
        }
    }
    bool enougnManaTocast(Card card)
    {
        float result = player.Mana - card.CardCost;

        if (result > 0)
        {
            return true;
        }
        else if (result == 0)
        {
            return true;
        }
        else if (result < 0)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
    void ActionBarInput()
    {
        if (!PlayerController.Instance.dead)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && !PlayerController.Instance.cloaked && !PlayerController.Instance.takingAction)
            {
                if (quickSlotContainers[0].color == enabledCard)
                {

                    if (quickSlots[0].GetCardInSlot() != null && enougnManaTocast(quickSlots[0].GetCardInSlot()))
                    {
                        quickSlots[0].UseCard();
                    }

                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && !PlayerController.Instance.cloaked && !PlayerController.Instance.takingAction)
            {
                if (quickSlotContainers[1].color == enabledCard)
                {
                    if (quickSlots[1].GetCardInSlot() != null && enougnManaTocast(quickSlots[1].GetCardInSlot()))
                    {
                        quickSlots[1].UseCard();
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && !PlayerController.Instance.cloaked && !PlayerController.Instance.takingAction)
            {
                if (quickSlotContainers[2].color == enabledCard)
                {
                    if (quickSlots[2].GetCardInSlot() != null && enougnManaTocast(quickSlots[2].GetCardInSlot()))
                    {
                        quickSlots[2].UseCard();
                    }

                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && !PlayerController.Instance.cloaked && !PlayerController.Instance.takingAction)
            {
                if (quickSlots[3].GetCardInSlot() != null && quickSlotContainers[3].color == enabledCard)
                {
                    if (enougnManaTocast(quickSlots[3].GetCardInSlot()))
                    {
                        quickSlots[3].UseCard();
                    }

                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5) && !PlayerController.Instance.cloaked && !PlayerController.Instance.takingAction)
            {
                if (quickSlotContainers[4].color == enabledCard)
                {
                    if (quickSlots[4].GetCardInSlot() != null && enougnManaTocast(quickSlots[4].GetCardInSlot()))
                    {
                        quickSlots[4].UseCard();
                    }

                }
            }
        }
    }
    #endregion

    #region Tooltip logic
    public void ToolTipOn(CardSlot slot)
    {
        toolTip.SetActive(true);
        Vector2 newPos;
        Vector3 mousePosOffsetted = new Vector3(Input.mousePosition.x, Input.mousePosition.y + toolTipRect.sizeDelta.y, Input.mousePosition.z);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, mousePosOffsetted, myCanvas.worldCamera, out newPos);
        toolTipRect.localPosition = newPos;
        if (slot.GetCardInSlot() != null)
        {
            toolTipText.text = slot.GetCardInSlot().GetDescription();
        }

    }

    public void ToolTipOff()
    {
        toolTip.SetActive(false);
    }
    #endregion
}
