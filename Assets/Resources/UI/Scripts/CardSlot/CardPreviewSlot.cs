﻿using UnityEngine;
using UnityEngine.UI;

public class CardPreviewSlot : MonoBehaviour {

    [SerializeField]
    CardContainer cardContainer;
    Image slot;
    Sprite originalSlot;
    Sprite currentCard;
    [SerializeField]
    Text cardName;
    [SerializeField]
    Text cardDescription;
    [SerializeField]
    Text cardUses;

    void Start()
    {
        Init();
    }
    	
	// Update is called once per frame
	void Update () {

        ShowCurrentCard();
	
	}

    void Init()
    {
        slot = gameObject.GetComponent<Image>();
        originalSlot = slot.sprite;
    }

    void ShowCurrentCard()
    {

        if (cardContainer.CurrentCard != null)
        {
            if (slot.sprite != cardContainer.CurrentCard)
            {
                slot.sprite = cardContainer.CurrentCard;
                cardName.text = cardContainer.CurrentCardName;
                cardDescription.text = cardContainer.CurrentlySelectedCardDesc;
                cardUses.text = cardContainer.CurrentlySelectedCardUses;
            }
            
        }
        else
        {
            if (slot.sprite != originalSlot)
            {
                slot.sprite = originalSlot;
                cardName.text = string.Empty;
                cardDescription.text = string.Empty;
                cardUses.text = string.Empty;
            }
        }
    }
}
