﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum SlotType
{
    LibrarySlot,
    InventorySlot,
    ActionBarSlot
}


public class CardSlot : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private SlotType slotType;
    [SerializeField]
    private CardType slotAction;
    private Card storedCard;
    private Sprite originalSlotSprite;
    private int doubleClickTime;
    private Image cardInSlot;
    private Text cardName;
    private PlayerCombat player;
    private Text cardUses;


    // Use this for initialization
    void Start()
    {

        InitSlot();

    }

    // Update is called once per frame
    void Update()
    {

        UpdateSlot();

    }

    #region Card Slot Functinons

    void InitSlot()
    {
        if (slotType == SlotType.ActionBarSlot)
        {
            LoadBasicAbility();
        }
        cardInSlot = gameObject.GetComponent<Image>();
        originalSlotSprite = cardInSlot.sprite;
        gameObject.tag = slotType.ToString();
        cardName = gameObject.transform.GetChild(0).GetComponent<Text>();
        if (slotType == SlotType.ActionBarSlot)
        {
            player = GameObject.FindWithTag(PlayerController.Instance.PlayerTag).GetComponent<PlayerCombat>();
            cardUses = gameObject.transform.GetChild(1).GetComponent<Text>();
        }

    }

    public void LoadBasicAbility()
    {
        switch (slotAction)
        {
            case CardType.Damage:
                {
                    storedCard = CardDatabase.Instance.PlayerABactions[0];
                    break;
                }
            case CardType.Heal:
                {
                    storedCard = CardDatabase.Instance.PlayerABactions[1];
                    break;
                }

            case CardType.Buff:
                {
                    storedCard = CardDatabase.Instance.PlayerABactions[2];
                    break;
                }
            case CardType.Utility:
                {
                    storedCard = CardDatabase.Instance.PlayerABactions[3];
                    break;
                }

        }
    }
    public CardType SlotAction
    {
        get
        {
            return slotAction;
        }
    }

    public SlotType SlotType
    {
        get
        {
            return slotType;
        }
    }

    public void StoreCard(Card storedCard)
    {
        this.storedCard = storedCard;
    }

    public void UseCard()
    {

        GetCardInSlot().UseCard(PlayerController.Instance.PlayerTarget);
        player.Mana -= GetCardInSlot().CardCost;
        if (GetCardInSlot().CardUses <= 0 && GetCardInSlot().Tier!=CardTier.Basic)
        {
            Discard();
            cardUses.text = string.Empty;
            LoadBasicAbility();
        }
       
    }

    public void Discard()
    {
        storedCard = null;
    }

    public Card GetCardInSlot()
    {
        return storedCard;
    }

    private void UpdateSlot()
    {
        if (storedCard != null)
        {
            gameObject.name = storedCard.GetName();
            if (slotType == SlotType.InventorySlot)
            {
                cardInSlot.sprite = storedCard.cardImg;
                cardName.text = storedCard.GetName();
            }
            else
            {
                cardInSlot.sprite = storedCard.cardImg;
                cardName.text = storedCard.GetName();
                if (GetCardInSlot().Tier != CardTier.Basic)
                {
                    cardUses.text = storedCard.CardUses.ToString();
                }


            }
            
        }
        else
        {
            cardInSlot.sprite = originalSlotSprite;
            cardName.text = string.Empty;
            if (slotType == SlotType.ActionBarSlot)
            {
                cardUses.text = string.Empty;
            }
        }
    }
    #endregion

    
    #region IPointerClickHandler implementation

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {

           
        }

    }
    //call on every click can be used to read which button we clicked
    #endregion
    

}
