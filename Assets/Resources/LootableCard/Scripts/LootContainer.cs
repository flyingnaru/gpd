﻿using UnityEngine;
using System.Collections;

public class LootContainer : MonoBehaviour {

    private bool ignoreCollisions;
    CardTier lootTier;
    Card loot;
    CardContainer inventory;
    bool lootable;
    [SerializeField]
    Material outlinedLootFBMat;
    [SerializeField]
    Material outlinedLootSidesMat;
    Material originalLootFBMat;
    Material originalLooSidesMat;
    Renderer lootRenderer;
    bool selected;
    PlayerCBLT lootText;

    // Use this for initialization
    void Awake()
    {
        lootRenderer = gameObject.GetComponent<Renderer>();
        originalLootFBMat = lootRenderer.materials[1];
        originalLooSidesMat = lootRenderer.materials[0];
    }
    void OnEnable()
    {
      
        if (Application.loadedLevel > 0)
        {
            IgnoreSkeletonCollisions();
            inventory = GameObject.FindWithTag("InvAb").gameObject.GetComponent<CardContainer>();
            lootText = GameObject.FindWithTag("CBLT").gameObject.GetComponent<PlayerCBLT>();
            StartCoroutine(TTL());
        }

        if (lootable)
        {
            loot = new Card(CardDatabase.Instance.GetLoot(lootTier));
        }


        
    }

	
	// Update is called once per frame
	void Update () {

        Selected();
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == PlayerController.Instance.PlayerTag)
        {

            if (inventory.LoadInInventorySlot(loot))
            {
                lootText.displayLoot(true, loot.GetName());
                loot = null;
                LootObjectPool.ReturnToPool(0, gameObject);
            }
            else
            {
                lootText.displayLoot(false, loot.GetName());
            }

            
        }
    }

    void IgnoreSkeletonCollisions()
    {
        Collider lootObject = gameObject.GetComponent<Collider>();
        GameObject[] playerSkeleton = GameObject.FindGameObjectsWithTag(PlayerController.Instance.PlayerBodyTag);
        foreach (GameObject bone in playerSkeleton)
        {
            Physics.IgnoreCollision(lootObject, bone.GetComponent<Collider>());
        }
        
    }

    public bool Lootable
    {
        set
        {
            lootable = value;
        }
    }

    public CardTier LootTier
    {
        set
        {
            lootTier = value;
        }
    }

    IEnumerator TTL()
    {
        int counter=0;
        while (true)
        {
            yield return new WaitForSeconds(1);
            counter++;
            if (counter == 60)
            {
                LootObjectPool.ReturnToPool(0, gameObject);
                yield break;
            }
        }

    }

    void Selected()
    {
        if (PlayerController.Instance.PlayerTarget != null)
        {
            if (!selected && PlayerController.Instance.PlayerTarget.GetInstanceID() == gameObject.GetInstanceID())
            {
                lootRenderer.materials[0] = outlinedLootSidesMat;
                lootRenderer.materials[1] = outlinedLootFBMat;
                lootRenderer.materials[2] = outlinedLootFBMat;
                selected = true;
            }
            else if (selected && PlayerController.Instance.PlayerTarget.GetInstanceID() != gameObject.GetInstanceID())
            {

                lootRenderer.materials[0] = originalLooSidesMat;
                lootRenderer.materials[1] = originalLootFBMat;
                lootRenderer.materials[2] = originalLootFBMat;
                selected = false;
            }
        }
        else if (selected && PlayerController.Instance.PlayerTarget == null)
        {
            lootRenderer.materials[0] = originalLooSidesMat;
            lootRenderer.materials[1] = originalLootFBMat;
            lootRenderer.materials[2] = originalLootFBMat;
            selected = false;
        }
    }


}
