﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnOverTime : MonoBehaviour {

    [SerializeField]
    int spawnRate;
    [SerializeField]
    GameObject denialPrefab;
    [SerializeField]
    GameObject angerPrefab;
    [SerializeField]
    GameObject bargainingPrefab;
    [SerializeField]
    GameObject depressionPrefab;
    List<GameObject> spawnPoints;
    List<GameObject> prefabList;
    bool finishedFirstBatch;
	// Use this for initialization
	void Awake () {

 
        GrabSpawnPoints();
        SpawnFirstBatch();
        
	}

    void Start()
    {
        StartSpawner();
    }

    void GrabSpawnPoints()
    {

        spawnPoints = new List<GameObject>();
        prefabList = new List<GameObject>();

        prefabList.Add(denialPrefab);
        prefabList.Add(angerPrefab);
        prefabList.Add(bargainingPrefab);
        prefabList.Add(depressionPrefab);

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            spawnPoints.Add(gameObject.transform.GetChild(i).gameObject);
        }
    }

    void SpawnFirstBatch()
    {
        int count = 0;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            int npcType = Random.Range(0, prefabList.Count);
            GameObject cNpc = Instantiate(prefabList[npcType], spawnPoints[i].transform.position, spawnPoints[i].transform.rotation) as GameObject;
            cNpc.transform.SetParent(spawnPoints[i].transform);
            count++;

        }

        if (count == gameObject.transform.childCount)
        {
            finishedFirstBatch = true;
        }
    }
    void StartSpawner()
    {
        StartCoroutine(SpawnNPCsOverTime(spawnRate));
    }

    IEnumerator SpawnNPCsOverTime(int rate)
    {
        while (true)
        {
            if (finishedFirstBatch)
            {
                int randomPoint = Random.Range(0, spawnPoints.Count);
                int npcType = Random.Range(0, prefabList.Count);
                RecursiveSpawning(randomPoint, npcType);
            }


            yield return new WaitForSeconds(rate);
        }

        //IMPROVE THIS PLEASE
    }


    bool FullySpawned()
    {
        int fullSpawn = 0;

        foreach (GameObject spawnPoint in spawnPoints)
        {
            if (spawnPoint.transform.childCount > 0)
            {
                fullSpawn++;
            }
        }
        if (fullSpawn == spawnPoints.Count)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    void RecursiveSpawning(int spawnP, int npc)
    {
        if (!FullySpawned())
        {
            if (spawnPoints[spawnP].transform.childCount == 0)
            {
                GameObject cNpc = Instantiate(prefabList[npc], spawnPoints[spawnP].transform.position, spawnPoints[spawnP].transform.rotation) as GameObject;
                cNpc.transform.SetParent(spawnPoints[spawnP].transform);
            }
            else
            {
                int newSpawnP = Random.Range(0, spawnPoints.Count);
                RecursiveSpawning(newSpawnP, npc);
            }
        }
        else
        {
            return;
        }

    }
}