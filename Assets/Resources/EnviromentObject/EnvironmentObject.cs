﻿using UnityEngine;
using System.Collections;

enum EnvironmentObjectType
{
    Mist,
    Log,
    Rock
}

public class EnvironmentObject : MonoBehaviour {

    [SerializeField]
    EnvironmentObjectType type;
    [SerializeField]
    int fadeCounter;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
    }

    void InitEnvObject()
    {

    }

    public void ClearEnviromentObject()
    {
        gameObject.SetActive(false);
    }


    //Improve after BETA
    IEnumerator ClearObjectSlowly()
    {
        yield return new WaitForSeconds(0);
    }
}
