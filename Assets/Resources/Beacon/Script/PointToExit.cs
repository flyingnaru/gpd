﻿using UnityEngine;
using System.Collections;

public class PointToExit : MonoBehaviour {

    GameObject exit;
    int level;
    [SerializeField]
    float rotationSpeed;
	// Use this for initialization
	void Start () {

        level = Application.loadedLevel;
	
	}
	
	// Update is called once per frame
	void Update () {

        FindAndPoint();
	
	}

    void FindAndPoint()
    {
        if (Application.loadedLevel != Application.levelCount - 1)
        {
            if (exit == null)
            {
                exit = GameObject.FindGameObjectWithTag("Entrance").gameObject;
            }
            else if (level != Application.loadedLevel && level < Application.levelCount - 1)
            {
                exit = GameObject.FindGameObjectWithTag("Entrance").gameObject;
            }

            if (exit != null)
            {
                Vector3 lookPosition = exit.transform.position - gameObject.transform.position;
                lookPosition.y = 0;
                Quaternion lookRotation = Quaternion.LookRotation(lookPosition);
                gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
       
    }
}
