﻿using UnityEngine;
using System.Collections;

public class PlayerWeapon : MonoBehaviour {
    private NpcCombat target;
    private int minDamage;
    private int maxDamage;
    private bool attacking;
    // Use this for initialization
    void Start()
    {

    }

    #region Weapon Functionality
    float RandomDamage()
    {
        return Random.Range(minDamage, maxDamage + 1);
    }

    #endregion

    #region Setters
    public int MaxDamage
    {
        set
        {
            maxDamage = value;
        }
    }

    public int MinDamage
    {
        set
        {
            minDamage = value;
        }
    }

    public bool Attacking
    {
        set
        {
            attacking = value;
        }
        get
        {
            return attacking;
        }
    }

    #endregion

    #region Collision Handling
    // Update is called once per frame
    void OnTriggerEnter(Collider c)
    {
        if (PlayerController.Instance.PlayerTarget != null)
        {

            if (c.gameObject.GetInstanceID() == PlayerController.Instance.PlayerTarget.GetInstanceID())
            {
                if (attacking)
                {
                    float dmg = RandomDamage();
                    c.gameObject.GetComponent<NpcCombat>().ReceiveDamage(dmg);
                    AudioEventManager.Instance.TriggerEvent(AudioEvent.NpcHit);
                    attacking = false;
                }

            }

        }
        else
        {
            attacking = false;
        }
    }
    #endregion
}
