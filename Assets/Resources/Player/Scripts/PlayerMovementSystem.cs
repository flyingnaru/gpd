using UnityEngine;
using System.Collections;

public class PlayerMovementSystem : MonoBehaviour
{

    private Vector3 clickPosition;
    private RaycastHit rcHit;
    private Ray ray;
    [SerializeField]
    float acceleration;
    [SerializeField]
    float turnRate;
    [SerializeField]
    float nonCombatStopingDistance;
    [SerializeField]
    float CombatStopingdistance;
    [SerializeField]
    float lootingStoppingDistance;
    [SerializeField]
    float endBossDistance;
    float stopingDistance;
    [SerializeField]
    bool destinationReached;
    bool grabPosition;
    private Animator pcAnim;
    [SerializeField]
    LayerMask layer;
    [SerializeField]
    GameObject beacon;
    bool chasingEnemy;
    bool chaseInProgress;
    GameObject chasedTarget;

    // Use this for initialization
    void Start()
    {

        InitSystem();
        //initialise the click position as the players position

    }


    // Update is called once per frame
    void FixedUpdate()
    {
         ContinousRayCasting();
    }
    void Update()
    {
       
        PlayerMovement();
        ResetMovement();
        ShowHideBeacon();
    }


    #region Movement Control Functions
    void InitSystem()
    {
        //get different componenets used by the player object
        clickPosition = gameObject.transform.position;
        gameObject.tag = PlayerController.Instance.PlayerTag;
        pcAnim = gameObject.GetComponent<Animator>();
        stopingDistance = nonCombatStopingDistance;
    }


    void ShowHideBeacon()
    {
        if (Input.GetKeyUp(KeyCode.B))
        {
            if (beacon.activeSelf)
            {
                beacon.SetActive(false);
            }
            else
            {
                beacon.SetActive(true);
            }
        }
    }
    // player movement mechanic
    void PlayerMovement()
    {
        //determine if the player can move
        //also if the player is dead he cannot move...I dont like zombies okay?
        if (!PlayerController.Instance.dead)
        {
            RotateAndMove();

            if ((Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) && !PlayerController.Instance.takingAction && !PlayerController.Instance.usingUI)
            {
                destinationReached = false;
                //when left click is pressed this will locate where the player clicked on the terrain
                grabPosition = true;

            }
            else
            {
                grabPosition = false;
            }


                //move player to the position
                
            
          
            
        }

    }

    void ContinousRayCasting()
    {
       
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //draws a line from the camera to the point in space where the mouse is 
        if (Physics.Raycast(ray, out rcHit, 1000,layer))
        {
            if (rcHit.collider.gameObject.tag != NpcHostileController.Instance.NpcTag && rcHit.collider.gameObject.tag != PlayerController.Instance.PlayerObstacleTag
                && rcHit.collider.gameObject.tag != PlayerController.Instance.PlayerLootTag && rcHit.collider.gameObject.tag != PlayerController.Instance.PlayerBosstag && rcHit.collider.gameObject.tag != PlayerController.Instance.PlayerEntranceTag)
            {

                if (PlayerController.Instance.PlayerTarget != null)
                {
                    PlayerController.Instance.PlayerTarget = null;
                }

                if (grabPosition)
                {
                    clickPosition = new Vector3(rcHit.point.x, gameObject.transform.position.y, rcHit.point.z);
                    stopingDistance = nonCombatStopingDistance;
                }

                //store click position
            }
            else if (rcHit.collider.gameObject.tag == PlayerController.Instance.PlayerEntranceTag)
            {
                PlayerController.Instance.PlayerTarget = rcHit.collider.gameObject;
                if (grabPosition)
                {
                    clickPosition = new Vector3(rcHit.point.x, gameObject.transform.position.y, rcHit.point.z);
                    stopingDistance = nonCombatStopingDistance;
                }

            }
            else if (rcHit.collider.gameObject.tag == PlayerController.Instance.PlayerBosstag)
            {
                PlayerController.Instance.PlayerTarget = rcHit.collider.gameObject;


                if (chasedTarget == null)
                {
                    chasedTarget = rcHit.collider.gameObject;
                }

                if (grabPosition)
                {
                    clickPosition = new Vector3(rcHit.collider.gameObject.transform.position.x, gameObject.transform.position.y, rcHit.collider.gameObject.transform.position.z);
                    stopingDistance = endBossDistance;
                    chasingEnemy = true;
                    if (!chaseInProgress)
                    {
                        StartCoroutine(TrackAndMoveToEnemy(chasedTarget));
                    }

                }
            }
            else if (rcHit.collider.gameObject.tag == NpcHostileController.Instance.NpcTag)
            {

                PlayerController.Instance.PlayerTarget = rcHit.collider.gameObject;


                if (chasedTarget == null)
                {
                    chasedTarget = rcHit.collider.gameObject;
                }

                if (grabPosition)
                {
                    clickPosition = new Vector3(rcHit.collider.gameObject.transform.position.x, gameObject.transform.position.y, rcHit.collider.gameObject.transform.position.z);
                    stopingDistance = CombatStopingdistance;
                    chasingEnemy = true;
                    if (!chaseInProgress)
                    {
                        StartCoroutine(TrackAndMoveToEnemy(chasedTarget));
                    }

                }

            }
            else if (rcHit.collider.gameObject.tag == PlayerController.Instance.PlayerLootTag)
            {
                PlayerController.Instance.PlayerTarget = rcHit.collider.gameObject;


                if (grabPosition)
                {
                    clickPosition = new Vector3(rcHit.point.x, gameObject.transform.position.y, rcHit.point.z);
                    stopingDistance = lootingStoppingDistance;
                }

            }
            else if (rcHit.collider.gameObject.tag == PlayerController.Instance.PlayerObstacleTag)
            {

                PlayerController.Instance.PlayerTarget = rcHit.collider.gameObject;

                if (grabPosition)
                {
                    clickPosition = gameObject.transform.position;
                    stopingDistance = nonCombatStopingDistance;
                }
            }


        }

    }

    void ResetMovement()
    {
        if (PlayerController.Instance.resetMovement)
        {
            MovementExecution(false);
            clickPosition = gameObject.transform.position;
            destinationReached = true;
            chasingEnemy = false;
            chasedTarget = null;
            PlayerController.Instance.takingAction = false;
            PlayerController.Instance.resetMovement = false;
        }
    }
    void RotateAndMove()
    {
        if (PlayerController.Instance.takingAction || PlayerController.Instance.stunned)
        {
            MovementExecution(false);
            clickPosition = gameObject.transform.position;
            destinationReached = true;
            chasingEnemy = false;
            chasedTarget = null;
        }
        else
        {
            //this function is used to handle movement and rotation
            if (Vector3.Distance(gameObject.transform.position, clickPosition) >= stopingDistance && !destinationReached)
            {
                //check if distance between object and destination is less than one
                Quaternion playerRotation = Quaternion.LookRotation(clickPosition - transform.position);
                //rotates the player object by substracting its position from the mouse position
                playerRotation.x = 0f;
                playerRotation.z = 0f;
                //removes x and z axis so we can move the object on its Y axis
                gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, playerRotation, Time.deltaTime * turnRate);
                MovementExecution(true);
            }
            else if (Vector3.Distance(gameObject.transform.position, clickPosition) < stopingDistance && !destinationReached)
            {
                MovementExecution(false);
                clickPosition = gameObject.transform.position;
                destinationReached = true;
                chasingEnemy = false;
                chasedTarget = null;
            }
        }



        

    }

    void MovementExecution(bool state)
    { //this function controls the rate at which the character accelerates and decelerates
        switch (state)
        {
            case true:
                {
                    if(pcAnim.GetFloat("Speed") < 1)
                    {
                        float tmpSpd = pcAnim.GetFloat("Speed");
                        pcAnim.SetFloat("Speed", tmpSpd+ acceleration * Time.deltaTime);
                    }
                    if (pcAnim.GetFloat("Speed") <= 0.5f)
                    {
                        AudioEventManager.Instance.TriggerEvent(AudioEvent.PlayerWalk);
                    }
                    else if(pcAnim.GetFloat("Speed") > 0.5f)
                    {
                        AudioEventManager.Instance.TriggerEvent(AudioEvent.PlayerRun);
                    }
                    break;
                }
            case false:
                {
                    pcAnim.SetFloat("Speed", 0);
                    break;
                }
        }
    }

    IEnumerator TrackAndMoveToEnemy(GameObject enemy)
    {
        while (chasingEnemy)
        {
            chaseInProgress = true;
            clickPosition = new Vector3(enemy.transform.position.x, gameObject.transform.position.y, enemy.transform.position.z);
            yield return new WaitForEndOfFrame();
            if (PlayerController.Instance.PlayerTarget != null)
            {
                if (PlayerController.Instance.PlayerTarget.GetInstanceID() != enemy.GetInstanceID())
                {
                    chaseInProgress = false;
                    chasingEnemy = false;
                    yield break;
                }
            }
            else
            {
                chaseInProgress = false;
                chasingEnemy = false;
                yield break;
            }
        }

        if (!chasingEnemy)
        {
            chaseInProgress = false;
            yield break;
        }

    }
    /*void GroundedCheck()
    {
        //This function casts a ray at the ground with a max length of 0.1 in order to check if the character is grounded or not


        if (Physics.Raycast(this.gameObject.transform.position, -Vector3.up, out groundCheckLine,0.1f))
        {

            PlayerController.Instance.isGrounded = true;
            Debug.Log("Plane Below");

        }

        else
        {
            PlayerController.Instance.isGrounded = false;
            Debug.Log("Nothing Below");
        }


    } */


    #endregion

}
