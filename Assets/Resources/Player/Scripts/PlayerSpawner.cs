﻿using UnityEngine;
using System.Collections;

public enum Structure
{
    Player,
    Camera
}

public class PlayerSpawner : MonoBehaviour {
    int lastLevel;
    [SerializeField]
    Structure consctruct;
	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(gameObject);
        lastLevel = Application.loadedLevel;

	}
	


	// Update is called once per frame
	void Update () {

        GoToSpawnPoint();
	
	}

    void GoToSpawnPoint()
    {
        int newLastLevel = Application.loadedLevel;
        if (lastLevel < newLastLevel || PlayerController.Instance.revive)
        {
            lastLevel = newLastLevel;
            GameObject spawnPoint = GameObject.FindGameObjectWithTag("PlayerSpawn");
            switch (consctruct)
            {
                case Structure.Camera:
                    {
                        gameObject.transform.position = new Vector3(spawnPoint.transform.position.x,gameObject.transform.position.y,spawnPoint.transform.position.z);
                        break;
                    }
                case Structure.Player:
                    {
                        gameObject.transform.position = spawnPoint.transform.position;
                        PlayerController.Instance.resetMovement = true;
                        break;
                    }
            }
        }
    }
}
