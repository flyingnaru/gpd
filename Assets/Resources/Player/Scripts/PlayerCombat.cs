﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void instantDelegate(float parameter);
public delegate IEnumerator instantDelegateTimer(int parameter);
public delegate IEnumerator overTimeDelegate(float parameter, int _parameter); 

public class PlayerCombat : MonoBehaviour {

    [SerializeField]
    GameObject playerSpellCaster;
    [SerializeField]
    private Material OriginalBody;
    [SerializeField]
    private Material OriginalWpn1;
    [SerializeField]
    private Material OriginalWpn2;
    [SerializeField]
    private Material cloakedBody;
    [SerializeField]
    private Material cloakedWpn;
    [SerializeField]
    AnimationClip two_h_cast;
    [SerializeField]
    AnimationClip combat_one_h_cast;
    [SerializeField]
    AnimationClip basic_attack;
    [SerializeField]
    private float playerRange;
    [SerializeField]
    [Range(1, 1000)]
    private int maxPlayerHealth=1;
    private float playerHealth;
    [SerializeField]
    [Range(1, 1000)]
    private int maxPlayerMana=1;
    private float playerMana;
    [SerializeField]
    private float manaPerSecond;
    [SerializeField]
    [Range(1,10)]
    private int outOfCombatRegenMultiplier=1;
    private float manaRegen;
    [SerializeField]
    [Range(1, 1000)]
    private int minDamage=1;
    [SerializeField]
    [Range(1, 1000)]
    private int maxDamage=1;
    private float damageModPercentage;
    private float drModPercentage;
    private Animator pcAnim;
    private RaycastHit positionLine;
    private Ray ray;
    [SerializeField]
    PlayerCBLT playerFloatingTxts;
    private Vector3 destinationPosition;
    private List<SkinnedMeshRenderer> playerRenderers;
    private List<GameObject> weaponSockets;
    private GameObject playerWeapon;
    private PlayerWeapon playerWeaponData;
    private MeshRenderer weaponRenderer;
    private Material[] cloakedWeaponMaterials;
    private Material[] originalWeaponMaterials;
    private bool materialsSwitched;
    private bool weaponDrawn;
    private bool attacking;
    private bool canAtk;
    private bool bleed;

    // private Coroutine playerDoTed;
    //  private Coroutine playerHoTed;
    // Use this for initialization
    void Start() {

        InitCombatSystem();


    }

    // Update is called once per frame
    void Update() {

        Death();
        Revive();
        CombatState();
        RegenState();
        WeaponUpdate();
        SheatheUnSheatheWeapon();
        BasicAttack();
        

    }

    #region Init
    void InitCombatSystem()
    {
        canAtk = true;
        playerHealth = maxPlayerHealth;
        playerMana = maxPlayerMana;
        StartCoroutine(ResourceRegeneration());
        pcAnim = gameObject.GetComponent<Animator>();
        playerWeapon = GameObject.FindGameObjectWithTag(PlayerController.Instance.PlayerWeaponTag);
        playerWeaponData = playerWeapon.GetComponent<PlayerWeapon>();
        weaponRenderer = playerWeapon.GetComponent<MeshRenderer>();
        originalWeaponMaterials = weaponRenderer.materials;
        cloakedWeaponMaterials = new Material[weaponRenderer.materials.Length];
        cloakedWeaponMaterials[0] = cloakedWpn;
        cloakedWeaponMaterials[1] = cloakedWpn;
        playerRenderers = new List<SkinnedMeshRenderer>();
        for (int i = 0; i < gameObject.transform.childCount - 3; i++)
        {
            playerRenderers.Add(gameObject.transform.GetChild(i).GetComponent<SkinnedMeshRenderer>());
        }
        weaponSockets = new List<GameObject>();
        foreach (GameObject socket in GameObject.FindGameObjectsWithTag(PlayerController.Instance.WeaponSocketTag))
        {
            weaponSockets.Add(socket);
        }
    }
    #endregion

    #region basic Combat Mechanics
    void WeaponUpdate()
    {
        playerWeaponData.MinDamage = minDamage + (int)(minDamage * damageModPercentage);
        playerWeaponData.MaxDamage = maxDamage + (int)(maxDamage * damageModPercentage);

    }

    void SheatheUnSheatheWeapon()
    {
        if (pcAnim.GetCurrentAnimatorStateInfo(0).IsTag("Combat") && !weaponDrawn)
        {
            for (int a = 0; a < weaponSockets.Count; a++)
            {
                if (playerWeapon.transform.parent.gameObject.GetInstanceID() != weaponSockets[a].GetInstanceID())
                {
                    playerWeapon.transform.SetParent(null);
                    playerWeapon.transform.SetParent(weaponSockets[a].gameObject.transform);
                    playerWeapon.transform.localPosition = new Vector3(0, 0, 0);
                    playerWeapon.transform.localRotation = Quaternion.identity;
                    weaponDrawn = true;
                    return;
                }


            }


        }
        else if (pcAnim.GetCurrentAnimatorStateInfo(0).IsTag("Non-Combat") && weaponDrawn)
        {
            for (int a = 0; a < weaponSockets.Count; a++)
            {
                if (playerWeapon.transform.parent.gameObject.GetInstanceID() != weaponSockets[a].GetInstanceID())
                {
                    playerWeapon.transform.SetParent(null);
                    playerWeapon.transform.SetParent(weaponSockets[a].gameObject.transform);
                    playerWeapon.transform.localPosition = new Vector3(0, 0, 0);
                    playerWeapon.transform.localRotation = Quaternion.identity;
                    weaponDrawn = false;
                    return;
                }


            }

        }
    }

    void CombatState()
    {
        if (PlayerController.Instance.inCombat)
        {
            pcAnim.SetBool("Sheathed", false);
        }
        else
        {
            pcAnim.SetBool("Sheathed", true);
        }
    }

    bool InAttackRange()
    {
        if (PlayerController.Instance.PlayerTarget != null)
        {
            if (Vector3.Distance(gameObject.transform.position, PlayerController.Instance.PlayerTarget.transform.position) <= playerRange)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    void BasicAttack()
    {


        if (canAtk)
        {
            StartCoroutine(BA(basic_attack.length));
        }
        
        /*
        if ((Input.GetMouseButton(0) || Input.GetMouseButtonDown(0)))
        {
            if (PlayerController.Instance.PlayerTarget != null && PlayerController.Instance.inCombat)
            {

                //   Debug.Log(PlayerController.Instance.PlayerTarget.name);
                if (InAttackRange() && !pcAnim.GetCurrentAnimatorStateInfo(0).IsName("Basic Attack") && !attacking)
                {
                    pcAnim.SetFloat("Speed", 0.0f);
                    gameObject.transform.LookAt(PlayerController.Instance.PlayerTarget.transform.position);
                    pcAnim.SetTrigger("Basic Attack");
                    playerWeaponData.Attacking = true;
                    attacking = true;
                    Debug.Log("Trigger BA");
                    AudioEventManager.Instance.TriggerEvent(AudioEvent.PlayerAxeAttack);
                }

                if (pcAnim.GetCurrentAnimatorStateInfo(0).IsName("Basic Attack") && attacking)
                {
                    pcAnim.ResetTrigger("Basic Attack");
                    
                    attacking = false;
                }


            }
            else
            {
                attacking = false;
            }

        }
        else if (InAttackRange() && pcAnim.GetCurrentAnimatorStateInfo(0).IsName("Basic Attack") && attacking)
        {
            pcAnim.ResetTrigger("Basic Attack");
            attacking = false;
            Debug.Log("Reset BA");
        }
        */
    }

    void RegenState()
    {
        if (PlayerController.Instance.inCombat)
        {
            manaRegen = manaPerSecond;
         }
        else
        {
            manaRegen = manaPerSecond * outOfCombatRegenMultiplier;
        } 
    }

    void Death()
    {
        if (!PlayerController.Instance.dead && !PlayerController.Instance.revive)
        {
            if (playerHealth < 0)
            {
                playerHealth = 0;
            }
            if (playerHealth == 0)
            {
                PlayerController.Instance.dead = true;
                pcAnim.SetTrigger("Death");
                gameObject.GetComponent<Rigidbody>().isKinematic = true;
                AudioEventManager.Instance.TriggerEvent(AudioEvent.PlayerDie);
            }
        }

    }

    void Revive()
    {
        if (PlayerController.Instance.revive)
        {

            playerHealth = maxPlayerHealth;
            playerMana = maxPlayerMana;
            PlayerController.Instance.dead = false;
            pcAnim.SetTrigger("Revive");
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            StartCoroutine(ResourceRegeneration());
            PlayerController.Instance.revive = false;
            PlayerController.Instance.usingUI = false;
            PlayerController.Instance.resetMovement = true;

        }
    }
    #endregion

    #region Instant Mechanics
    public void ReceiveDamage(float hpAmmount)
    {
        if (playerHealth > 0)
        {
            if (drModPercentage > 0)
            {
                hpAmmount = hpAmmount - ((hpAmmount * drModPercentage) / 100);
            }
            playerHealth = playerHealth - hpAmmount;
            playerFloatingTxts.displayTakenDmg(hpAmmount);
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(10, gameObject.transform);
            pEffect.transform.position = gameObject.transform.position + new Vector3(0, 1, 0);
            pEffect.transform.rotation = gameObject.transform.rotation;
            pEffect = null;

           


        }
    }

    public void Heal(float hpAmmount)
    {
        if (playerHealth <= maxPlayerHealth)
        {
            playerFloatingTxts.displayReceivedHeal(hpAmmount);
            playerHealth = playerHealth + hpAmmount;

        }

        if (playerHealth > maxPlayerHealth)
        {
            playerHealth = maxPlayerHealth;
        }
    }

    void HealMP(float mpAmmount)
    {
        if (playerMana < maxPlayerMana)
        {
            playerMana += mpAmmount;
        }

        if (playerMana > maxPlayerMana)
        {
            playerMana = maxPlayerMana;
        }
    }

    public void CastedHeal(float hpAmmount)
    {
        pcAnim.SetBool("2HCast", true);

        if (!pcAnim.GetCurrentAnimatorStateInfo(0).IsName("2HCastSpell"))
        {
            StartCoroutine(CastTimeInstantAction(AudioEvent.PlayerCastCardLong,two_h_cast.length, hpAmmount, Heal));
        }

    }

    #endregion

    #region Over Time Mechanics

    public void Stealth(int stealthTime)
    {
        pcAnim.SetBool("1HCombatCast", true);

        if (!pcAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(CastTimeTimerAction(AudioEvent.PlayerCastCardShort, combat_one_h_cast.length,stealthTime,StartStealth));
        }
    }

    public void DoTPlayer(float hpAmmount, int dotTime)
    {

        StartCoroutine(DoT(hpAmmount / dotTime, dotTime));
    }

    public void HoTPlayer(float hpAmmount,int hotTime)
    {

        pcAnim.SetBool("1HCombatCast", true);

        if (!pcAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort, combat_one_h_cast.length, hpAmmount, hotTime, HoT));
        }
    }

    public void StunPlayer(int stunTime)
    {
        StartCoroutine(Stuned(stunTime));
    }

    public void PlayerDRMod(float DRPercentage, int DRTime)
    {
        pcAnim.SetBool("1HCombatCast", true);

        if (!pcAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort, combat_one_h_cast.length, drModPercentage, DRTime, DamageReductionMod));
        }
    }

    public void PlayerDMGMod(float DMGPercentage,int DMGTime)
    {
        pcAnim.SetBool("1HCombatCast", true);

        if (!pcAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort, combat_one_h_cast.length, DMGPercentage, DMGTime, DamageMod));
        }
    }
    #endregion

    #region IEnumerators

    public IEnumerator CastTimeInstantAction(AudioEvent castType,float castTime, float hpAmmount, instantDelegate instantActionDelegate)
    {
        if (pcAnim.GetBool("Sheathed"))
        {
            pcAnim.SetBool("Sheathed", false);
        }

        PlayerController.Instance.takingAction = true;
        AudioEventManager.Instance.TriggerEvent(castType);
        if (castTime < two_h_cast.length)
        {
            if (instantActionDelegate.Method.Name == "ReceiveDamage")
            {
                GameObject pEffect = ParticleEffectsPool.GrabFromPool(0, gameObject.transform);
                pEffect.transform.SetParent(playerSpellCaster.transform);
                pEffect.transform.localPosition = Vector3.zero;
                pEffect.transform.localRotation = Quaternion.identity;
            }
           


        }
        else
        {
            if (instantActionDelegate.Method.Name=="Heal")
            {
                GameObject pEffect = ParticleEffectsPool.GrabFromPool(3, gameObject.transform);
                pEffect.transform.SetParent(gameObject.transform);
                pEffect.transform.localPosition = Vector3.zero;
                pEffect.transform.localRotation = Quaternion.identity;
                pEffect.transform.localEulerAngles = new Vector3(-90, 0, 0);
            }
  
        }
       
        yield return new WaitForSeconds(castTime);
        instantActionDelegate(hpAmmount);
        PlayerController.Instance.takingAction = false;
        
    }

    public IEnumerator CastTimeOverTimeAction(AudioEvent castType, float castTime, float hpAmmount, int duration,overTimeDelegate overTimeActionDelegate)
    {
        if (pcAnim.GetBool("Sheathed"))
        {
            pcAnim.SetBool("Sheathed", false);
        }
        PlayerController.Instance.takingAction = true;


        if (overTimeActionDelegate.Method.Name == "DoT")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(6, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }
        else if (overTimeActionDelegate.Method.Name == "HoT")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(4, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }
        else if (overTimeActionDelegate.Method.Name == "DamageReductionMod")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(2, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }
        else if (overTimeActionDelegate.Method.Name == "DamageMod")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(1, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }


        yield return new WaitForSeconds(castTime);
        AudioEventManager.Instance.TriggerEvent(castType);
        StartCoroutine(overTimeActionDelegate(hpAmmount/duration,duration));
        PlayerController.Instance.takingAction = false;

    }

    public IEnumerator CastTimeTimerAction(AudioEvent castType,float castTime, int duration, instantDelegateTimer timerActionDelegate)
    {

        if (pcAnim.GetBool("Sheathed"))
        {
            pcAnim.SetBool("Sheathed", false);
        }
        PlayerController.Instance.takingAction = true;
        AudioEventManager.Instance.TriggerEvent(castType);

        if (timerActionDelegate.Method.Name == "Stuned")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(7, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }
        else if (timerActionDelegate.Method.Name == "StartStealth")
        {
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(8, gameObject.transform);
            pEffect.transform.SetParent(playerSpellCaster.transform);
            pEffect.transform.localPosition = Vector3.zero;
            pEffect.transform.localRotation = Quaternion.identity;
        }

        yield return new WaitForSeconds(castTime);
        StartCoroutine(timerActionDelegate(duration));
        PlayerController.Instance.takingAction = false;

    }

    IEnumerator ResourceRegeneration()
    {
        while (!PlayerController.Instance.dead)
        {
            HealMP(manaRegen);
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator HoT(float ammount, int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            Heal(ammount);
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                yield break;
            }
        }
    }

    IEnumerator Stuned(int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            PlayerController.Instance.stunned = true;
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            Debug.Log("Ticked");
            if (time == 0)
            {
                PlayerController.Instance.stunned = false;
                Debug.Log("Broken");
                yield break;
            }
        }

    }

    IEnumerator DoT(float damage, int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            ReceiveDamage(damage);
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                yield break;
            }
        }

    }

    IEnumerator DamageReductionMod(float percentage, int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            drModPercentage = percentage;
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                drModPercentage = 0;
                yield break;
            }
        }

    }

    IEnumerator DamageMod(float percentage, int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            damageModPercentage = percentage;
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                damageModPercentage = 0;
                yield break;
            }
        }

    }

    IEnumerator StartStealth(int time)
    {
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            PlayerController.Instance.cloaked = true;
            if (!materialsSwitched)
            {
                weaponRenderer.materials = cloakedWeaponMaterials;
                foreach (SkinnedMeshRenderer rend in playerRenderers)
                {
                    rend.material = cloakedBody;

                }
                materialsSwitched = true;
            }
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                PlayerController.Instance.cloaked = false;
                if (materialsSwitched)
                {
                    weaponRenderer.materials = originalWeaponMaterials;
                    foreach (SkinnedMeshRenderer rend in playerRenderers)
                    {
                        rend.material = OriginalBody;
                    }
                    materialsSwitched = false;
                }
                yield break;
            }
        }

    }

    
    IEnumerator BA(float atkLength)
    {
        canAtk = false;
        
        if ((Input.GetMouseButton(0) || Input.GetMouseButtonDown(0)) && !PlayerController.Instance.takingAction)
        {
            PlayerController.Instance.takingAction = true;
            Debug.Log("atk");
            if (PlayerController.Instance.PlayerTarget != null && (PlayerController.Instance.PlayerTarget.tag == NpcHostileController.Instance.NpcTag || PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerBosstag) && PlayerController.Instance.inCombat)
            {
                //   Debug.Log(PlayerController.Instance.PlayerTarget.name);
                if (InAttackRange() && !pcAnim.GetCurrentAnimatorStateInfo(0).IsName("Basic Attack") && !attacking)
                {
                    gameObject.transform.LookAt(PlayerController.Instance.PlayerTarget.transform.position);
                    pcAnim.SetTrigger("Basic Attack");

                    Debug.Log("Trigger BA");
                    yield return new WaitForSeconds(atkLength / 3);
                    playerWeaponData.Attacking = true;
                    attacking = true;
                    AudioEventManager.Instance.TriggerEvent(AudioEvent.PlayerAxeAttack);
                    yield return new WaitForSeconds(atkLength / 6);
                    canAtk = true;
                    PlayerController.Instance.takingAction = false;
                    attacking = false;
                }
                else if (pcAnim.GetCurrentAnimatorStateInfo(0).IsName("Basic Attack") && attacking)
                {

                    pcAnim.ResetTrigger("Basic Attack");
                    attacking = false;
                    PlayerController.Instance.takingAction = false;
                    canAtk = true;


                }

            }
            else if (PlayerController.Instance.PlayerTarget == null && PlayerController.Instance.inCombat)
            {
                attacking = false;
                PlayerController.Instance.takingAction = false;
                canAtk = true;
            }
            else if (!InAttackRange())
            {
                attacking = false;
                PlayerController.Instance.takingAction = false;
                canAtk = true;
            }
            else
            {
                attacking = false;
                PlayerController.Instance.takingAction = false;
                canAtk = true;
            }


            canAtk = true;
            PlayerController.Instance.takingAction = false;
        }
        canAtk = true;

    }
    
    #endregion

    #region Getters/Setters
    public int MaxHealth
    {
        get
        {
            return maxPlayerHealth;
        }
    }

    public float Health
    {
        get
        {
            return playerHealth;
        }
    }

    public int MaxMana
    {
        get
        {
            return maxPlayerMana;
        }
    }

    public float Mana
    {
        get
        {
            return playerMana;
        }
        set
        {
            playerMana = value;
        }
    }

    public Animator PCAnim
    {
        get
        {
            return pcAnim;
        }
    }

    public float OneHCastTime
    {
        get
        {
            return combat_one_h_cast.length;
        }
    }

    public float TwoHCastTime
    {
        get
        {
            return two_h_cast.length;
        }
    }
    #endregion
}
