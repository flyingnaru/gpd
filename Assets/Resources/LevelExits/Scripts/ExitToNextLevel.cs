﻿using UnityEngine;
using System.Collections;

public class ExitToNextLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == PlayerController.Instance.PlayerBodyTag)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
            PlayerController.Instance.inCombat = false;
            NpcHostileController.Empower();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Application.LoadLevel(Application.loadedLevel + 1);
            PlayerController.Instance.inCombat = false;
            NpcHostileController.Empower();
        }
    }
}
