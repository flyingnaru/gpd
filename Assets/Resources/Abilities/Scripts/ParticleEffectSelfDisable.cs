﻿using UnityEngine;
using System.Collections;

public class ParticleEffectSelfDisable : MonoBehaviour {

    ParticleSystem pSys;
    private int index;
	// Use this for initialization
	void OnEnable () {

        Init();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (!pSys.IsAlive())
        {
            ParticleEffectsPool.ReturnToPool(index, gameObject);
        }
	
	}

    void Init()
    {
        pSys = gameObject.GetComponent<ParticleSystem>();
        pSys.Play();
    }

    public int ParticleIndexNumber
    {
        set
        {
            index = value;
        }
    }
}
