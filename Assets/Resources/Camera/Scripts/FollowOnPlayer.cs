﻿using UnityEngine;
using System.Collections;

public class FollowOnPlayer : MonoBehaviour {

    private GameObject player;
    [SerializeField]
    private float offset;
    [SerializeField]
    private float cameraMoveSpeed;
    private bool canMove;

    void Start() {

        player = GameObject.FindWithTag(PlayerController.Instance.PlayerTag);
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        //Follow Player Function

        if (player != null && canMove)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, new Vector3(player.transform.position.x, gameObject.transform.position.y, player.transform.position.z - offset), cameraMoveSpeed * Time.deltaTime);
        }
        else if (!canMove)
        {
            if (Vector3.Distance(gameObject.transform.position, player.transform.position) < 7.6f)
            {
                gameObject.transform.position = gameObject.transform.position;
            }
            else
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, new Vector3(player.transform.position.x, gameObject.transform.position.y, player.transform.position.z - offset), cameraMoveSpeed * Time.deltaTime);
            }
            
        }

    }


    void OnTriggerStay(Collider col)
    {
        canMove = true;
    }
    void OnTriggerEnter(Collider col)
    {
        canMove = true;
    }

    void OnTriggerExit(Collider col)
    {
        canMove = false;    
    }
}
