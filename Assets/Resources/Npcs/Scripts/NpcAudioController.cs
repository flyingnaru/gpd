﻿using UnityEngine;
using System.Collections.Generic;

public class NpcAudioController : MonoBehaviour {

    AudioSource npcASCombat;
    AudioSource npcASOutOfCombat;
    [SerializeField]
    AudioClip npcCast;
    [SerializeField]
    AudioClip npcWalk;
    [SerializeField]
    AudioClip npcDie;
    [SerializeField]
    AudioClip npcBite;
    [SerializeField]
    AudioClip npcHitPlayer;
    [SerializeField]
    List<AudioClip> npcIdle;
	// Use this for initialization
	void Awake () {

        npcASCombat = gameObject.transform.GetChild(0).GetComponent<AudioSource>();
        npcASOutOfCombat = gameObject.transform.GetChild(1).GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    #region NPC Events
    public void NpcCast()
    {

    }


    public void NpcBreathe()
    {
        if (!npcASOutOfCombat.isPlaying)
        {
            int randomVal = Random.Range(0, 4);
            npcASOutOfCombat.PlayOneShot(npcIdle[randomVal]);
        }
    }

    public void NpcBite()
    {
        npcASCombat.PlayOneShot(npcBite);
    }

   public void NpcDie()
    {
        npcASCombat.PlayOneShot(npcDie);
    }

   public void NpcWalk()
    {
        if (!npcASOutOfCombat.isPlaying)
        {
            npcASOutOfCombat.PlayOneShot(npcWalk);
        }
    }

    public void NpcHitPlayer()
    {
        npcASCombat.PlayOneShot(npcHitPlayer);
    }
    #endregion

}
