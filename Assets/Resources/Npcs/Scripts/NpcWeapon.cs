﻿using UnityEngine;
using System.Collections;

public class NpcWeapon : MonoBehaviour {
    private PlayerCombat player;
    private NpcAudioController npcAudio;
    private float minDamage;
    private float maxDamage;
    private bool dead;
    private bool hit;
	// Use this for initialization
	void Start () {
	
	}

    #region Weapon Functionality
    float RandomDamage()
    {
        return Random.Range(minDamage, maxDamage + 1);
    }
    
    #endregion

    #region Collision Handling
    // Update is called once per frame
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == PlayerController.Instance.PlayerTag && !dead)
        {
            if (player == null)
            {
                player = c.gameObject.GetComponent<PlayerCombat>();

            }
            if (hit)
            {
                player.ReceiveDamage(RandomDamage());
                npcAudio.NpcHitPlayer();
                hit = false;
            }

            
        }
    }
    #endregion

    #region Setter/Getter
    public bool Dead
    {
        get
        {
            return dead;
        }

        set
        {
            dead = value;
        }

    }

    public float MaxDamage
    {
        set
        {
            maxDamage = value;
        }
    }

    public float MinDamage
    {
        set
        {
            minDamage = value;
        }
    }

    public NpcAudioController NPCAudio
    {
        set
        {
            npcAudio = value;
        }
    }

    public bool Hit
    {
        set
        {
            hit = value;
        }
    }
    #endregion
}
