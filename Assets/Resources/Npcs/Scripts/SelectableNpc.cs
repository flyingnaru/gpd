﻿using UnityEngine;
using System.Collections;

public class SelectableNpc : MonoBehaviour {

    [SerializeField]
    Material outlinedNpcMat;
    Material originalNpcMat;
    Renderer[] renderers;
    bool selected;
    private NpcBaseAI npcStatus;
	
    // Use this for initialization
	void Start () {

        InitSelectable();
	
	}
	
	// Update is called once per frame
	void Update () {

        Selected();
	}

    void InitSelectable()
    {
        renderers = GetComponentsInChildren<Renderer>();
        originalNpcMat = renderers[0].material;
        npcStatus = gameObject.GetComponent<NpcBaseAI>();
    }

    void Selected()
    {
        if(PlayerController.Instance.PlayerTarget!= null)
        {
            if (!selected && PlayerController.Instance.PlayerTarget.GetInstanceID() == gameObject.GetInstanceID())
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.material = outlinedNpcMat;
                }
                selected = true;
            }
            else if (selected && PlayerController.Instance.PlayerTarget.GetInstanceID() != gameObject.GetInstanceID())
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.material = originalNpcMat;
                }
                selected = false;
            }
        }
        else if (selected && PlayerController.Instance.PlayerTarget==null)
        {
            foreach (Renderer renderer in renderers)
            {
                renderer.material = originalNpcMat;
            }
            selected = false;
        }
    }
}
