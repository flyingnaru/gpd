﻿using UnityEngine;
using System.Collections;

public delegate void npcInstantDelegate(float parameter);
public delegate IEnumerator npcInstantDelegateTimer(int parameter);
public delegate IEnumerator npcOverTimeDelegate(float parameter, int _parameter);

public class NpcCombat : MonoBehaviour
{
    [SerializeField]
    NpcStages NPCType;
    [SerializeField]
    CardTier lootTier;
    [SerializeField]
    NpcHUD hud;
    private float maxHealth;
    private float health;
    private float damageModPercentage;
    private float drModPercentage;
    private Animator npcAnim;
    private NpcBaseAI npcBaseAi;
    private PlayerCombat playerTarget;
    public NpcWeapon weapon;
    private bool dead;
    private bool executedPlayer;
    [SerializeField]
    AnimationClip npcCast;
    [SerializeField]
    NpcAudioController npcAudio;
    private float npcSkillvalue;
    private int npcSkillDuration;
    private bool attacking;
    // Use this for initialization
    void Start()
    {
        InitNpc();

    }

    // Update is called once per frame
    void Update()
    {
        basicAttack();
        executePlayer();
        Dead();
    }
    #region Init
    void InitNpc()
    {
        weapon.NPCAudio = npcAudio;
        switch (NPCType)
        {
            case NpcStages.Denial:
                {
                    maxHealth = NpcHostileController.Instance.Denial.MaxHealth;
                    weapon.MinDamage = NpcHostileController.Instance.Denial.MinDamage;
                    weapon.MaxDamage = NpcHostileController.Instance.Denial.MaxDamage;
                    npcSkillvalue = NpcHostileController.Instance.Denial.MaxDamage;
                    npcSkillDuration = NpcHostileController.Instance.Denial.AbilityDuration;
                    lootTier = NpcHostileController.Instance.Denial.L33tLewts;
                    break;
                }
            case NpcStages.Anger:
                {
                    maxHealth = NpcHostileController.Instance.Anger.MaxHealth;
                    weapon.MinDamage = NpcHostileController.Instance.Anger.MinDamage;
                    weapon.MaxDamage = NpcHostileController.Instance.Anger.MaxDamage;
                    npcSkillvalue = NpcHostileController.Instance.Anger.MaxDamage;
                    npcSkillDuration = NpcHostileController.Instance.Anger.AbilityDuration;
                    lootTier = NpcHostileController.Instance.Anger.L33tLewts;
                    break;
                }
            case NpcStages.Bargaining:
                {
                    maxHealth = NpcHostileController.Instance.Bargaining.MaxHealth;
                    weapon.MinDamage = NpcHostileController.Instance.Bargaining.MinDamage;
                    weapon.MaxDamage = NpcHostileController.Instance.Bargaining.MaxDamage;
                    npcSkillvalue = NpcHostileController.Instance.Bargaining.MaxDamage;
                    npcSkillDuration = NpcHostileController.Instance.Bargaining.AbilityDuration;
                    lootTier = NpcHostileController.Instance.Bargaining.L33tLewts;
                    break;
                }
            case NpcStages.Depression:
                {
                    maxHealth = NpcHostileController.Instance.Depression.MaxHealth;
                    weapon.MinDamage = NpcHostileController.Instance.Depression.MinDamage;
                    weapon.MaxDamage = NpcHostileController.Instance.Depression.MaxDamage;
                    npcSkillvalue = NpcHostileController.Instance.Depression.MaxDamage;
                    npcSkillDuration = NpcHostileController.Instance.Depression.AbilityDuration;
                    lootTier = NpcHostileController.Instance.Depression.L33tLewts;
                    break;
                }
        }

        health = maxHealth;
        npcAnim = gameObject.GetComponent<Animator>();
        npcBaseAi = gameObject.GetComponent<NpcBaseAI>();
        playerTarget = PlayerController.Instance.Player.GetComponent<PlayerCombat>();
    }
    #endregion

    #region Combat
    void basicAttack()
    {

        if (!npcBaseAi.Stunned)
        {
            if (npcBaseAi.PlayerReached && !PlayerController.Instance.cloaked && !PlayerController.Instance.dead && !dead)
            {
                if (npcAnim.GetCurrentAnimatorStateInfo(0).IsTag("Combat") == false && !attacking)
                {
                    npcAnim.SetTrigger("Attack");
                    attacking = true;
                    weapon.Hit = true;
                }
                else if (npcAnim.GetCurrentAnimatorStateInfo(0).IsTag("Combat") && attacking)
                {
                    npcAnim.ResetTrigger("Attack");
                    attacking = false;
                }
            }
            else
            {
                attacking = false;
            }
        }


    }

    void executePlayer()
    {
        if (PlayerController.Instance.dead && !dead && npcBaseAi.PlayerReached && !executedPlayer)
        {
            npcAnim.SetTrigger("DevourPlayer");
            npcAudio.NpcBite();
            executedPlayer = true;
        }
    }

    void DoTPlayer()
    {
        npcAnim.SetTrigger("CastAttack");
        playerTarget.DoTPlayer(npcSkillvalue / npcSkillDuration, npcSkillDuration);
    }

    void StunPlayer()
    {
        npcAnim.SetTrigger("CastAttack");
        playerTarget.StunPlayer(npcSkillDuration);
    }

    void HotSelf()
    {
        npcAnim.SetTrigger("CastAttack");
        HoTNpc(npcSkillvalue, npcSkillDuration);
    }

    void HealSelf()
    {
        npcAnim.SetTrigger("CastAttack");
        Heal(npcSkillvalue);
    }

    void Dead()
    {
        if (health < 0 && !dead)
        {
            health = 0;
        }

        if (health <= 0 && !dead)
        {
            npcAnim.applyRootMotion = true;
            npcBaseAi.Dead = true;
            npcAnim.SetTrigger("Death");
            dead = true;
            weapon.Dead = true;
            npcAudio.NpcDie();
            if (!PlayerController.Instance.firstLootSpawned)
            {
                PlayerController.Instance.firstLootSpawned = true;
            }
            if (gameObject.tag != PlayerController.Instance.PlayerBosstag)
            {
                LootObjectPool.GrabFromPool(0, gameObject.transform, lootTier);
            }

            StartCoroutine(Decay());
            if (PlayerController.Instance.PlayerTarget.GetInstanceID() == gameObject.GetInstanceID())
            {
                PlayerController.Instance.PlayerTarget = null;
            } 
        }
    }
    #endregion

    #region Instant Mechanics
    public void ReceiveDamage(float hpAmmount)
    {
        if (health > 0)
        {
            if (drModPercentage > 0)
            {
                hpAmmount = hpAmmount - ((hpAmmount * drModPercentage) / 100);
            }
            health = health - hpAmmount;
            hud.displayTakenDmg(hpAmmount);
            GameObject pEffect = ParticleEffectsPool.GrabFromPool(10, gameObject.transform);
            pEffect.transform.position = gameObject.transform.position + new Vector3(0, 1, 0);
            pEffect.transform.rotation = gameObject.transform.rotation;
            pEffect = null;
        }
    }

    public void CastedReceiveDamage(float hpAmmount)
    {
        playerTarget.PCAnim.SetBool("1HCombatCast", true);
        playerTarget.transform.LookAt(gameObject.transform.position);
        if (!playerTarget.PCAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(playerTarget.CastTimeInstantAction(AudioEvent.PlayerCastCardShort,playerTarget.OneHCastTime, hpAmmount, ReceiveDamage));
            
        }
    }


    public void Heal(float hpAmmount)
    {
        health = health + hpAmmount;
    }

    #endregion

    #region Getters
    public float getHealth()
    {
        return this.health;
    }

    public float getMaxHealth()
    {
        return this.maxHealth;
    }

    public NpcStages NpcType
    {
        get
        {
            return NPCType;
        }
    }

    #endregion

    #region Over Time Mechanics

    public void DoTNpc(float hpDamage,int dotTime)
    {
        playerTarget.PCAnim.SetBool("1HCombatCast", true);

        if (!playerTarget.PCAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(playerTarget.CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort,playerTarget.OneHCastTime, hpDamage, dotTime, DoT));
        }
           
    }

    public void StunNpc(int stunTime)
    {
        playerTarget.PCAnim.SetBool("1HCombatCast", true);
        playerTarget.transform.LookAt(gameObject.transform.position);
        if (!playerTarget.PCAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
        {
            StartCoroutine(playerTarget.CastTimeTimerAction(AudioEvent.PlayerCastCardShort,playerTarget.OneHCastTime, stunTime, Stuned));
        }
       
    }
                                                                      //I DONT WANNA WORK TODAY!
    public void NPCDRMod(float percentage,int time)
    {
        if (percentage > 0)
        {
            npcAnim.SetTrigger("CardAttack");
            StartCoroutine(DamageReductionMod(percentage, time));
        }
        else if (percentage < 0)
        {
            playerTarget.PCAnim.SetBool("1HCombatCast", true);

            if (!playerTarget.PCAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
            {
                StartCoroutine(playerTarget.CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort,playerTarget.OneHCastTime, percentage, time,DamageReductionMod));
            }
        }

    }

    public void DamageMod(float percentage, int time)
    {
        if (percentage > 0)
        {
            npcAnim.SetTrigger("CardAttack");
            StartCoroutine(DmgMod(percentage, time));
        }
        else if (percentage < 0)
        {
            playerTarget.PCAnim.SetBool("1HCombatCast", true);

            if (!playerTarget.PCAnim.GetCurrentAnimatorStateInfo(0).IsName("1HCombatCastSpell"))
            {
                StartCoroutine(playerTarget.CastTimeOverTimeAction(AudioEvent.PlayerCastCardShort, playerTarget.OneHCastTime, percentage, time,DmgMod));
            }
        }

    }

    void HoTNpc(float hpAmmount, int hotTime)
    {
        StartCoroutine(HoT(hpAmmount / hotTime, hotTime));
    }
    #endregion

    #region IEnumerators


    IEnumerator HoT(float ammount, int time)
    {
        npcAnim.SetTrigger("CastAttack");

        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            Heal(ammount);
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                yield break;
            }
        }
    }

    IEnumerator Stuned(int time)
    {

        while (time > 0)
        {
            time--;
            Debug.Log(time);
            npcBaseAi.Stunned = true;
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            Debug.Log("Ticked");
            if (time == 0)
            {
                npcBaseAi.Stunned = false;
                Debug.Log("Broken");
                yield break;
            }
        }


    }

    IEnumerator DoT(float damage, int time)
    {


        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            ReceiveDamage(damage);
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                yield break;
            }
        }

    }

    IEnumerator DamageReductionMod(float percentage, int time)
    {
        if (percentage > 0)
        {
            npcAnim.SetTrigger("CastAttack");
        }
        else if (percentage < 0)
        {
            PlayerController.Instance.inCombat = true;
            PlayerController.Instance.takingAction = true;
            playerTarget.PCAnim.SetTrigger("1HCombatCast");
            yield return new WaitForSeconds(playerTarget.OneHCastTime);
            PlayerController.Instance.takingAction = false;
        }
        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            drModPercentage = percentage;
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                drModPercentage = 0;
                yield break;
            }
        }

    }

    IEnumerator DmgMod(float percentage, int time)
    {
        if (percentage > 0)
        {
            npcAnim.SetTrigger("CastAttack");
        }
        else if (percentage < 0)
        {
            PlayerController.Instance.inCombat = true;
            PlayerController.Instance.takingAction = true;
            playerTarget.PCAnim.SetTrigger("1HCombatCast");
            yield return new WaitForSeconds(playerTarget.OneHCastTime);
            PlayerController.Instance.takingAction = false;
        }

        while (time > 0)
        {
            time--;
            Debug.Log(time);
            yield return new WaitForSeconds(1);
            Debug.Log(time);
            damageModPercentage = percentage;
            Debug.Log("Ticked");
            if (time == 0)
            {
                Debug.Log("Broken");
                damageModPercentage = 0;
                yield break;
            }
        }

    }

    IEnumerator Decay()
    {
        int count=0;
        while (true)
        {
            yield return new WaitForSeconds(1);
            count++;
            if (count == 5)
            {
                Destroy(gameObject);
                yield break;
            }
        }
    }
    #endregion

}
