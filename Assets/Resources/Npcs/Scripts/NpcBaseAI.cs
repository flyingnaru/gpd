﻿using UnityEngine;
using System.Collections;

public class NpcBaseAI : MonoBehaviour
{
    [SerializeField]
    NpcAudioController npcAudio;
    NavMeshAgent NPCAgentMode;
    Animator npcAnim;
    GameObject player;
    private CapsuleCollider npcCollider;
    [SerializeField]
    private float detectionRange;
    [SerializeField]
    private float rotationSpeed;
    private bool inCombat;
    private bool chasing;
    private bool reached;
    private bool dead;
    private bool stunned;
    private bool deAggro;
    // Use this for initialization
    void Awake()
    {

        Init();

    }

    // Update is called once per frame
    void Update()
    {
        Idle();
        DetectPlayer(player);
        LookAtPlayer(player);
        FollowPlayer(player);

    }

    #region Init
    void Init()
    {
        NPCAgentMode = gameObject.GetComponent<NavMeshAgent>();
        npcAnim = gameObject.GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag(PlayerController.Instance.PlayerTag);
        npcCollider = gameObject.GetComponent<CapsuleCollider>();
    }
    #endregion
    #region Aggro
    void Idle()
    {
        if (!inCombat && !dead)
        {
            npcAudio.NpcBreathe();
        }
        
    }
    void DetectPlayer(GameObject targetPlayer)
    {
        if (!dead)
        {
            if (Vector3.Distance(NPCAgentMode.transform.position, player.transform.position) <= detectionRange && !PlayerController.Instance.cloaked && !PlayerController.Instance.dead)
            {
                inCombat = true;
                PlayerController.Instance.inCombat = true;
            }
            else if (PlayerController.Instance.dead)
            {
                inCombat = false;
                PlayerController.Instance.inCombat = false;
            }
            else if (PlayerController.Instance.cloaked)
            {
                PlayerController.Instance.inCombat = false;
            }
        }
        else
        {
            if (!deAggro)
            {
                PlayerController.Instance.inCombat = false;
                deAggro = true;
            }

        }

    }
    #endregion
    #region Movement
    void FollowPlayer(GameObject targetPlayer)
    {
        if (!dead)
        {
            if (inCombat)
            {
                Debug.Log(PlayerController.Instance.cloaked);
                if (PlayerController.Instance.cloaked || PlayerController.Instance.dead || stunned)
                {
                    npcAnim.SetFloat("Speed", 0.0f);
                    NPCAgentMode.Stop();
                    chasing = false;
                    inCombat = false;
                }
                else if (Vector3.Distance(gameObject.transform.position, targetPlayer.transform.position) <= NPCAgentMode.stoppingDistance)
                {
                    if (chasing)
                    {
                        npcAnim.SetFloat("Speed", 0.0f);
                        NPCAgentMode.Stop();
                        chasing = false;
                        reached = true;
                    }
                    else
                    {
                        reached = true;
                    }



                }
                else
                {

                    if (!chasing)
                    {

                        npcAnim.SetFloat("Speed", 0.5f);
                        NPCAgentMode.SetDestination(player.transform.position);
                        chasing = true;
                        reached = false;
                    }



                }
                if (npcAnim.GetFloat("Speed") > 0 && !reached && chasing && !GameStateController.Instance.Paused)
                    npcAudio.NpcWalk();
                


            }
            else if (stunned)
            {
                NPCAgentMode.Stop();
                npcAnim.SetFloat("Speed", 0.0f);
                reached = true;
                chasing = false;
            }
        }
        else
        {
            if (NPCAgentMode.enabled)
            {
                NPCAgentMode.Stop();
            }

            NPCAgentMode.enabled = false;
            npcCollider.enabled = false;

        }
    }

    void LookAtPlayer(GameObject targetPlayer)
    {
        if (PlayerController.Instance.inCombat && !PlayerController.Instance.cloaked && !dead && inCombat && !stunned)
        {
            Quaternion newRotation = Quaternion.LookRotation((targetPlayer.transform.position - NPCAgentMode.transform.position));
            newRotation.x = NPCAgentMode.transform.rotation.x;
            newRotation.z = NPCAgentMode.transform.rotation.z;
            gameObject.transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
        }


    }
    #endregion

    #region Setters Getters
    public bool PlayerReached
    {
        get
        {
            return reached;
        }
    }

    public bool Dead
    {
        set
        {
            dead = value;
        }
    }

    public bool Stunned
    {
        get
        {
            return stunned;
        }

        set
        {
            stunned = value;
        }
    }
    #endregion
}
