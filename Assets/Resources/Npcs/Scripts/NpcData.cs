﻿using UnityEngine;
using System.Collections.Generic;

public enum NpcStages
{
    None,
    Denial,
    Anger,
    Bargaining,
    Depression,
    Omni
}

public enum NPCAbility
{
    None,
    Heal,
    Stun,
    BuffDamage,
    BuffDamageResistance,
    DoT,
    HoT


}

[System.Serializable]
public class NpcData
{
    [SerializeField]
    private CardTier lootTier;
    [SerializeField]
    private int maxHealth;
    [SerializeField]
    [Range(1, 5)]
    private float hpMultiplier;
    [SerializeField]
    [Range(1, 100)]
    private float minDamage;
    [SerializeField]
    [Range(1, 100)]
    private float maxDamage;
    [SerializeField]
    [Range(1, 10)]
    private float damageMultiplier;
    [SerializeField]
    private int abilityDuration;
    public List<NPCAbility> abilities;

    public float MinDamage
    {
        get
        {
            if (damageMultiplier != 0)
            {
                return minDamage * damageMultiplier;
            }
            else
            {
                return minDamage;
            }
           
        }
    }

    public float MaxDamage
    {
        get
        {
            if (damageMultiplier != 0)
            {
                return maxDamage * damageMultiplier;
            }
            else
            {
                return maxDamage;
            }
        }
    }

    public float MaxHealth
    {
        get
        {
            if (hpMultiplier != 0)
            {
                return maxHealth * hpMultiplier;
            }
            else
            {
                return maxHealth;
            }
        }
    }

    public int AbilityDuration
    {
        get
        {
            return abilityDuration;
        }
    }

    public float HpMult
    {
        get
        {
            return hpMultiplier;
        }

        set
        {
            hpMultiplier = value;
        }
    }

    public float DmgMult
    {
        get
        {
            return damageMultiplier;
        }

        set
        {
            damageMultiplier = value;
        }
    }

    public CardTier L33tLewts
    {
        get
        {
            return lootTier;
        }

        set
        {
            lootTier = value;
        }
    }

}
