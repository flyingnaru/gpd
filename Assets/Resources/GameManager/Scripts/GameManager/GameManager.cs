﻿using UnityEngine;


public static class GameManager
{
    public static GameObject gameManager;


    public static GameObject LoadGameManager()
    {
        return gameManager;
    }

    public static void ReceiveGameManager(GameObject manager)
    {
        gameManager = manager;
    }
}

