﻿using UnityEngine;
using System.Collections;

public class GameManagerSpawner : MonoBehaviour {


    void Awake()
    {
        if (GameManager.LoadGameManager() != null)
        {
            gameObject.SetActive(false);
        }
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

}
