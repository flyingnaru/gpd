﻿using UnityEngine.Events;
using System.Collections.Generic;

public enum DialogEvents
{
    T,
    D1,
    D2,
    D3,
    D4,
    A1,
    A2,
    A3,
    A4,
    B1,
    B2,
    B3,
    B4,
    Dp1,
    Dp2,
    Dp3,
    Dp4,
    Ac
}


public class UIEventManager : Singleton<UIEventManager>
{

    private Dictionary<DialogEvents, UnityEvent> EventDatabase;
    private UnityEvent eventBuffer;
    // Use this for initialization
    void Awake()
    {
        this.InitEventDatabase();
    }


    #region Init Functions
    void InitEventDatabase()
    {
        if (this.EventDatabase == null)
        {
            this.EventDatabase = new Dictionary<DialogEvents, UnityEvent>();
        }
    }
    #endregion
    #region Event Subscription
    public void SubscribeEvent(DialogEvents eventDesignation, UnityAction listener)
    {
        this.eventBuffer = null;
        if (this.EventDatabase.TryGetValue(eventDesignation, out eventBuffer))
        {
            this.eventBuffer.AddListener(listener);
        }
        else
        {
            this.eventBuffer = new UnityEvent();
            this.eventBuffer.AddListener(listener);
            this.EventDatabase.Add(eventDesignation, this.eventBuffer);
        }
    }

    public void UnSubscribeEvent(DialogEvents eventDesignation, UnityAction listener)
    {
        if (!Instance)
        {
            return;
        }

        this.eventBuffer = null;
        if (this.EventDatabase.TryGetValue(eventDesignation, out this.eventBuffer))
        {
            this.eventBuffer.RemoveListener(listener);
        }


    }

    void UnSubscribeAllEvents()
    {
        foreach (DialogEvents uiEvt in this.EventDatabase.Keys)
        {
            EventDatabase[uiEvt].RemoveAllListeners();
        }
    }

    public void TriggerEvent(DialogEvents eventDesignation)
    {
        this.eventBuffer = null;

        if (this.EventDatabase.TryGetValue(eventDesignation, out this.eventBuffer))
        {
            this.eventBuffer.Invoke();
        }
    }
    #endregion
    void OnDisable()
    {
        this.UnSubscribeAllEvents();
    }
}

