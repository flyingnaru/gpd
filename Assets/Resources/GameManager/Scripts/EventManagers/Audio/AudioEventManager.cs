﻿using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System.Collections.Generic;

public enum AudioEvent
{
    Click,
    PlayerWalk,
    PlayerRun,
    PlayerHit,
    PlayerDie,
    PlayerCastCardLong,
    PlayerCastCardShort,
    PlayerAxeAttack,
    NpcHit
    
}


public class AudioEventManager : Singleton<AudioEventManager> {

    private Dictionary<AudioEvent, UnityEvent> EventDatabase;
    private UnityEvent eventBuffer;
	// Use this for initialization
	void Awake ()
    {
        this.InitEventDatabase();
    }
	
	// Update is called once per frame
	void Update () {
	}

    #region Init Functions
    void InitEventDatabase()
    {
        if (this.EventDatabase == null)
        {
            this.EventDatabase = new Dictionary<AudioEvent, UnityEvent>();
        }
    }
    #endregion
    #region Event Subscription
    public void SubscribeEvent(AudioEvent eventDesignation, UnityAction listener)
    {
        this.eventBuffer = null;
        if (this.EventDatabase.TryGetValue(eventDesignation, out eventBuffer))
        {
            this.eventBuffer.AddListener(listener);
        }
        else
        {
            this.eventBuffer = new UnityEvent();
            this.eventBuffer.AddListener(listener);
            this.EventDatabase.Add(eventDesignation, this.eventBuffer);
        }
    }

    public void UnSubscribeEvent(AudioEvent eventDesignation, UnityAction listener)
    {
        if (!Instance)
        {
            return;
        }

        this.eventBuffer = null;
        if (this.EventDatabase.TryGetValue(eventDesignation, out this.eventBuffer))
        {
            this.eventBuffer.RemoveListener(listener);
        }


    }

    void UnSubscribeAllEvents()
    {
        foreach (AudioEvent audioEvt in this.EventDatabase.Keys)
        {
            EventDatabase[audioEvt].RemoveAllListeners();
        }
    }

    public void TriggerEvent(AudioEvent eventDesignation)
    {
        this.eventBuffer = null;

        if (this.EventDatabase.TryGetValue(eventDesignation, out this.eventBuffer))
        {
            this.eventBuffer.Invoke();
        }
    }
    #endregion
    void OnDisable()
    {
       this.UnSubscribeAllEvents();
    }
}
