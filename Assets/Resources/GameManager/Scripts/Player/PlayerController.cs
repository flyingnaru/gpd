﻿using UnityEngine;
using System.Collections;




public class PlayerController : Singleton<PlayerController> {

    private string playerTag;
    private string weaponSocketTag;
    private string playerWeaponTag;
    private string playerBodyTag;
    private string playerObstacleTag;
    private string playerLootTag;
    private string playerEntranceTag;
    private string playerEndBossTag;
    private GameObject playerTarget;
    private GameObject player;
    public bool usingUI;
    public bool dead;
    public bool revive;
    public bool stunned;
    public bool takingAction;
    public bool cloaked;
    public bool inCombat;
    public bool weaponDraw;
    public bool resetMovement;
    public bool firstLootSpawned;
    public bool playerAcceptance;
    private Vector3 currentSpawnpoint;

    enum PlayerTags
    {
        Player,
        WeaponSocket,
        PlayerWeapon,
        PlayerBody,
        EndBoss
    }
    enum PlayerObjects
    {
        Lootable,
        Walls,
        Entrance
    }


    void OnLevelWasLoaded(int level)
    {
        if (level != 0 && level != Application.levelCount-1)
        {
            currentSpawnpoint = GameObject.FindGameObjectWithTag("Entrance").transform.position;
        }
    }

    protected PlayerController()
    {
        playerTag = PlayerTags.Player.ToString();
        weaponSocketTag = PlayerTags.WeaponSocket.ToString();
        playerWeaponTag = PlayerTags.PlayerWeapon.ToString();
        playerBodyTag = PlayerTags.PlayerBody.ToString();
        playerObstacleTag = PlayerObjects.Walls.ToString();
        playerLootTag = PlayerObjects.Lootable.ToString();
        playerEntranceTag = PlayerObjects.Entrance.ToString();
        playerEndBossTag = PlayerTags.EndBoss.ToString();
        //Defeat Instatiation 
    }

	// Use this for initialization
	void Awake () {

    }

	// Update is called once per frame
	void Update () {
	
	}

    public GameObject PlayerTarget
    {
        get
        {
            return playerTarget;
        }
        set
        {
            this.playerTarget = value;
        }
    }

    public GameObject Player
    {
        get
        {
            if (player != null)
            {
                return player;
            }
            else
            {
                player = GameObject.FindGameObjectWithTag(playerTag);
                return player;
            }

        }
    }

    public string PlayerTag
    {
        get
        {
            return this.playerTag;
        }
        
    }

    public string WeaponSocketTag
    {
        get
        {
            return this.weaponSocketTag;
        }
        
    }

    public string PlayerWeaponTag

    {
        get
        {
            return playerWeaponTag;
        }
    }

    public string PlayerBodyTag
    {
        get
        {
            return playerBodyTag;
        }
    }

    public string PlayerObstacleTag
    {
        get
        {
            return playerObstacleTag;
        }
    }

    public string PlayerLootTag
    {
        get
        {
            return playerLootTag;
        }
    }

    public string PlayerEntranceTag
    {
        get
        {
            return playerEntranceTag;
        }
    }

    public string PlayerBosstag
    {
        get
        {
            return playerEndBossTag;
        }

    }
}
