﻿using UnityEngine;
using System.Collections;

public class GameStateController : Singleton<GameStateController> {

    private float originalTS;
    private bool paused;
	// Use this for initialization
	void Awake () {

        originalTS = Time.timeScale;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void PauseUnPause()
    {
        if (!paused)
        {
            Time.timeScale = 0.0f;
            paused = true;

        }
        else
        {
            Time.timeScale = originalTS;
            paused = false;
        }
    }

    public static void TogglePause()
    {
        Instance.PauseUnPause();
    }

    public bool Paused
    {
        get
        {
            return paused;
        }
    }

}
