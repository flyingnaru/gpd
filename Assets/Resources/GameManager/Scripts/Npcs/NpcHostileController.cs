﻿using UnityEngine;
using System;
using System.Collections;

public enum NpcTags
{
    None,
    NpcHostile,
    NpcFriendly
}

public class NpcHostileController : Singleton<NpcHostileController> {

    [SerializeField]
    private NpcData denial;
    [SerializeField]
    private NpcData anger;
    [SerializeField]
    private NpcData depression;
    [SerializeField]
    private NpcData bargaining;
    private string hostileNpcTag;
    // Use this for initialization
    void Awake () {

        hostileNpcTag = NpcTags.NpcHostile.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void Empower()
    {
        Instance.denial.HpMult = Instance.denial.HpMult + (0.25f * Instance.denial.HpMult);
        Instance.bargaining.HpMult = Instance.bargaining.HpMult + (0.25f * Instance.bargaining.HpMult);
        Instance.anger.HpMult = Instance.anger.HpMult + (0.25f * Instance.anger.HpMult);
        Instance.depression.HpMult = Instance.depression.HpMult + (0.25f * Instance.depression.HpMult);

        Instance.denial.DmgMult = Instance.denial.DmgMult + (0.25f * Instance.denial.DmgMult);
        Instance.anger.DmgMult = Instance.anger.DmgMult + (0.25f * Instance.anger.DmgMult);
        Instance.depression.DmgMult = Instance.depression.DmgMult + (0.25f * Instance.depression.DmgMult);
        Instance.bargaining.DmgMult = Instance.bargaining.DmgMult + (0.25f * Instance.bargaining.DmgMult);

        int denialCurrentLootTier = (int)Instance.denial.L33tLewts;
        Instance.denial.L33tLewts = (CardTier)denialCurrentLootTier + 1;
        int angerCurrentLootTier = (int)Instance.anger.L33tLewts;
        Instance.anger.L33tLewts = (CardTier)angerCurrentLootTier + 1;
        int depressionCurrentLootTier = (int)Instance.depression.L33tLewts;
        Instance.depression.L33tLewts = (CardTier)depressionCurrentLootTier + 1;
        int bargainingCurrentLootTier = (int)Instance.bargaining.L33tLewts;
        Instance.bargaining.L33tLewts = (CardTier)bargainingCurrentLootTier + 1;
    }
    #region getters
    public string NpcTag
    {
        get
        {
            return hostileNpcTag;
        }
    }
    public NpcData Denial
    {
        get
        {
            return denial;
        }
    }

    public NpcData Anger
    {
        get
        {
            return anger;
        }
    }

    public NpcData Depression
    {
        get
        {
            return depression;
        }
    }

    public NpcData Bargaining
    {
        get
        {
            return bargaining;
        }
    }

    #endregion
}
