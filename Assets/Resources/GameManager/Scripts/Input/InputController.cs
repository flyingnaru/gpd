﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class InputController : Singleton<InputController>{

    public bool doubleClicked;
    private float doubleClickStart=0;
    public float doubleClickCatchTime;


    void Awake()
    {
 
    }

    protected InputController()
    {
        //Defeat Initialization
    }
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

     //   HandleDoubleClicks();
       // CallMainMenu();
    }

    void HandleDoubleClicks()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!doubleClicked && Time.time <= (doubleClickStart + doubleClickCatchTime))
            {
                this.doubleClicked = true;
            }
            else
            {
                this.doubleClicked = false;
                this.doubleClickStart = Time.time;
            }

        }

        if (Input.GetMouseButtonUp(0) && this.doubleClicked)
        {
            doubleClicked = false;
        }



    }

    void CallMainMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          //  GUIController.MainMenu();
        }
    }







}
