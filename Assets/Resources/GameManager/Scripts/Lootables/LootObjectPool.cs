﻿using UnityEngine;
using System.Collections.Generic;

public class LootObjectPool : Singleton<LootObjectPool> {

    [SerializeField]
    int initialNumberOfInstances;
    [SerializeField]
    List<GameObject> prefabs;
    List<List<GameObject>> lootContainers;
    // Use this for initialization
    void Awake()
    {

        PopulatePool();

    }

    // Update is called once per frame
    void Update()
    {

    }


    void PopulatePool()
    {
        lootContainers = new List<List<GameObject>>();
        for (int a = 0; a < prefabs.Count; a++)
        {
            lootContainers.Add(new List<GameObject>());
            
            for (int b = 0; b < initialNumberOfInstances; b++)
            {
                GameObject obj = Instantiate(prefabs[a]) as GameObject;
                obj.transform.parent = gameObject.transform;
                obj.SetActive(false);
                DontDestroyOnLoad(obj);
                obj.GetComponent<LootContainer>().Lootable = true;
                lootContainers[a].Add(obj);

            }
           
        }


    }

    public static GameObject GrabFromPool(int index, Transform transform, CardTier tier)
    {
        for (int a = 0; a < Instance.lootContainers[index].Count; a++)
        {
            if (!Instance.lootContainers[index][a].activeSelf)
            {
                Instance.lootContainers[index][a].transform.position = transform.position;
                Instance.lootContainers[index][a].GetComponent<LootContainer>().LootTier = tier;
                Instance.lootContainers[index][a].SetActive(true);
                return Instance.lootContainers[index][a];
            }
        }

        GameObject newInstance = Instantiate(Instance.prefabs[index]) as GameObject;
        newInstance.SetActive(false);
        Instance.lootContainers[index].Add(newInstance);
        newInstance = null;
        Instance.lootContainers[index][Instance.lootContainers[index].Count - 1].transform.position = transform.position;
        Instance.lootContainers[index][Instance.lootContainers[index].Count - 1].GetComponent<LootContainer>().LootTier = tier;
        Instance.lootContainers[index][Instance.lootContainers[index].Count - 1].SetActive(true);
        return Instance.lootContainers[index][Instance.lootContainers[index].Count - 1];
    }

    public static void ReturnToPool(int index, GameObject poolObj)
    {
        for (int a = 0; a < Instance.lootContainers[index].Count; a++)
        {
            if (Instance.lootContainers[index][a].GetInstanceID() == poolObj.GetInstanceID())
            {
                poolObj.transform.parent = Instance.gameObject.transform;
                poolObj.SetActive(false);
            }
        }
    }


}
