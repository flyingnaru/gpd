﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIController : MonoBehaviour {
    //put other shit when needed
    [SerializeField]
    GameObject mainMenu;
    [SerializeField]
    GameObject playerFrameUI;
    [SerializeField]
    GameObject overlay;
    [SerializeField]
    GameObject endState;
    Slider playerHPbar;
    private float playerHPVal;
    Slider playerMPbar;
    private float playerMPVal;
    Text playerHP;
    Text playerMP;
    private bool mainMenuToggle;
    private PlayerCombat player;
    private bool calledEndstate;
    // Use this for initialization
    void Start() {

        InitHealthBars();

    }

    // Update is called once per frame
    void Update() {

        PlayerHealthBarUpdate();
        MainMenu();
        OverlayOnOff();
        DeathAndRebirth();

    }

    #region UI usage
    public void UsingUI()
    {
        PlayerController.Instance.usingUI = true;
    }

    public void StopUsingUI()
    {
        PlayerController.Instance.usingUI = false;
    }
    #endregion

    #region Escape Menu functions

    void ToggleMainMenu()
    {
        if (!mainMenuToggle)
        {
            PlayerController.Instance.usingUI = true;
            GameStateController.TogglePause();
            mainMenu.SetActive(true);
            mainMenuToggle = true;
        }
        else
        {
            PlayerController.Instance.usingUI = false;
            GameStateController.TogglePause();
            mainMenu.SetActive(false);
            mainMenuToggle = false;
        }
    }

    public void MainMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mainMenu != null)
            {
                ToggleMainMenu();
            }
        }

    }

    void DeathAndRebirth()
    {
        if (PlayerController.Instance.dead || PlayerController.Instance.playerAcceptance)
        {
            if (!calledEndstate)
            {
                StartCoroutine(DelayEndState());
            }

        }

        if (!PlayerController.Instance.dead && !PlayerController.Instance.playerAcceptance)
        {
            endState.SetActive(false);
            calledEndstate = false;
           
        }
    }

    IEnumerator DelayEndState()
    {
        calledEndstate = true;
        yield return new WaitForSeconds(10);
        endState.SetActive(true);
    }
    public void MainMenuResume()
    {
        ToggleMainMenu();
    }

    public void QuitToMainMenu()
    {
        ToggleMainMenu();

        DestroyObject(GameObject.FindWithTag(PlayerController.Instance.PlayerTag).gameObject,0.01f);
       // DestroyObject(GameObject.FindWithTag("GameManager").gameObject,0.01f);
        DestroyObject(Camera.main.gameObject, 0.01f);
        Application.LoadLevel(0);
    }
    #endregion

    #region Health Bars

    void InitHealthBars()
    {
        player = GameObject.FindGameObjectWithTag(PlayerController.Instance.PlayerTag).GetComponent<PlayerCombat>();
        playerHPbar = playerFrameUI.transform.GetChild(0).GetComponent<Slider>();
        playerHPVal = playerHPbar.value;
        playerMPbar = playerFrameUI.transform.GetChild(1).GetComponent<Slider>();
        playerMPVal = playerMPbar.value;
        playerHP = playerFrameUI.transform.GetChild(2).GetComponent<Text>();
        playerMP = playerFrameUI.transform.GetChild(3).GetComponent<Text>();
    }

    void PlayerHealthBarUpdate()
    {
        playerHPbar.value = playerHPVal * (player.Health / player.MaxHealth);
        playerMPbar.value = playerMPVal * (player.Mana / player.MaxMana);
        playerHP.text = (int)player.Health + " / " + player.MaxHealth;
        playerMP.text = (int)player.Mana + " / " + player.MaxMana;
    }

    #endregion

    #region Overlay
    void OverlayOnOff()
    {
        if (Input.GetKeyUp(KeyCode.H))
        {
            if (overlay.activeSelf)
            {
                overlay.SetActive(false);
            }
            else
            {
                overlay.SetActive(true);
            }
        }
    }
    #endregion
}
