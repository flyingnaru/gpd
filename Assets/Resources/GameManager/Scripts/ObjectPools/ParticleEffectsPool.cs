﻿using UnityEngine;
using System.Collections.Generic;

public class ParticleEffectsPool : Singleton<ParticleEffectsPool> {

    [SerializeField]
    int initialNumberOfInstances;
    [SerializeField]
    List<GameObject> prefabs;
    List<List<GameObject>> particleEffects;
    // Use this for initialization
    void Awake () {

        PopulatePool();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void PopulatePool()
    {
        particleEffects = new List<List<GameObject>>();
        for (int a = 0; a < prefabs.Count; a++)
        {
            particleEffects.Add(new List<GameObject>());
           
            for (int b = 0; b < initialNumberOfInstances; b++)
            {
                GameObject obj = Instantiate(prefabs[a]);
                obj.SetActive(false);
                DontDestroyOnLoad(obj);
                particleEffects[a].Add(obj);
                obj = null;
            }
          
        }
    }

    public static GameObject GrabFromPool(int index, Transform transform)
    {
        for (int a = 0; a < Instance.particleEffects[index].Count; a++)
        {
            if (!Instance.particleEffects[index][a].activeSelf)
            {
                Instance.particleEffects[index][a].transform.parent = transform;
                Instance.particleEffects[index][a].GetComponent<ParticleEffectSelfDisable>().ParticleIndexNumber = index;
                Instance.particleEffects[index][a].SetActive(true);
                return Instance.particleEffects[index][a];
            }
        }

        Instance.particleEffects[index].Add((GameObject)Instantiate(Instance.prefabs[index]));
        Instance.particleEffects[index][Instance.particleEffects[index].Count - 1].SetActive(true);
        return Instance.particleEffects[index][Instance.particleEffects[index].Count-1];
    }

    public static void ReturnToPool(int index,GameObject poolObj)
    {
        for (int a = 0; a < Instance.particleEffects[index].Count;a++)
        {
            if (Instance.particleEffects[index][a].GetInstanceID() == poolObj.GetInstanceID())
            {
                poolObj.transform.parent = Instance.gameObject.transform;
                poolObj.SetActive(false);
            }
        }
    }


}
