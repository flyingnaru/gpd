﻿using UnityEngine;
using System.Collections;

public class AudioController : Singleton<AudioController> {

    private AudioSource aSPlayerActions;
    private AudioSource aSPlayerMovement;

    [SerializeField]
    AudioClip playerWalkGrass;
    [SerializeField]
    AudioClip playerRunGrass;
    [SerializeField]
    AudioClip playerWalkGravel;
    [SerializeField]
    AudioClip playerRunGravel;
    [SerializeField]
    AudioClip playerAttack;
    [SerializeField]
    AudioClip playerGetsHit;
    [SerializeField]
    AudioClip playerDies;
    [SerializeField]
    AudioClip playerFastCast;
    [SerializeField]
    AudioClip playerLongCast;
    [SerializeField]
    AudioClip npcGetsHit;
    bool gravel;
    bool audioLoaded;

    void Awake()
    {
        aSPlayerActions = gameObject.transform.GetChild(0).GetComponent<AudioSource>();
        aSPlayerMovement = gameObject.transform.GetChild(1).GetComponent<AudioSource>();
    }
	// Use this for initialization
	void Start () {

        AudioEventManager.Instance.SubscribeEvent(AudioEvent.Click, Click);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerWalk, PlayerWalk);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerRun, PlayerRun);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerAxeAttack, PlayerAttack);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerCastCardShort, PlayerCastShort);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerCastCardLong, PlayerCastLong);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.PlayerDie, PlayerDie);
        AudioEventManager.Instance.SubscribeEvent(AudioEvent.NpcHit,PlayerHitNpc);


    }

	
	// Update is called once per frame
	void Update () {

	
	}

    void Click()
    {
        Debug.Log("Click!!");
    }

  
    #region Player Events
    void PlayerWalk()
    {
        if (!aSPlayerMovement.isPlaying)
        {
            
            if (isOnGravel)
            {
                aSPlayerMovement.PlayOneShot(playerWalkGravel);
            }
            else
            {
                aSPlayerMovement.PlayOneShot(playerWalkGrass);
            }
        }
    }

    void PlayerRun()
    {
        if (!aSPlayerMovement.isPlaying)
        {
            if (isOnGravel)
            {
                aSPlayerMovement.PlayOneShot(playerRunGravel);
            }
            else
            {
                aSPlayerMovement.PlayOneShot(playerRunGrass);
            }
        }
    }

    void PlayerDie()
    {
        aSPlayerActions.PlayOneShot(playerDies);
    }

    void PlayerCastShort()
    {
        aSPlayerActions.PlayOneShot(playerFastCast);
    }

    void PlayerCastLong()
    {
        aSPlayerActions.PlayOneShot(playerLongCast);
    }

    void PlayerAttack()
    {
        aSPlayerActions.PlayOneShot(playerAttack);
   
    }

    void PlayerHitNpc()
    {
        aSPlayerActions.PlayOneShot(npcGetsHit);
    }


    #endregion



    #region Setter/Getter
    public static bool isOnGravel
    {
        get
        {
            return Instance.gravel;
        }

        set
        {
            Instance.gravel = value;
        }
    }

    #endregion
}
