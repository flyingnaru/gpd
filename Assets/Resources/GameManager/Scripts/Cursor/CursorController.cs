﻿using UnityEngine;
using System.Collections.Generic;



public enum CursorState
{
    idle,
    idleCantClick,
    attack,
    loot,
    entrance
}

public class CursorController : Singleton<CursorController> {

    [SerializeField]
    List<Texture2D> cursorImages;
    bool clicked;

    CursorState currentState;

	// Use this for initialization
	void Start () {

        currentState = CursorState.idle;
	
	}
	
	// Update is called once per frame
	void Update () {
        getCurrentTarget();
        handleCursor();
	
	}

    void getCurrentTarget()
    {
        if (PlayerController.Instance.PlayerTarget != null)
        {
            if (PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerEntranceTag)
            {
                currentState = CursorState.entrance;
            }
            else if (PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerLootTag)
            {
                currentState = CursorState.loot;
            }
            else if (PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerObstacleTag)
            {
                currentState = CursorState.idleCantClick;
            }
            else if (PlayerController.Instance.PlayerTarget.tag == PlayerController.Instance.PlayerBosstag || PlayerController.Instance.PlayerTarget.tag == NpcHostileController.Instance.NpcTag)
            {
                currentState = CursorState.attack;
            }
        }
        else
        {
            currentState = CursorState.idle;
        }

    }
    void handleCursor()
    {
        if (currentState == CursorState.idle || PlayerController.Instance.usingUI)
        {
            if (clicked)
            {
                Cursor.SetCursor(cursorImages[1], Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(cursorImages[0], Vector2.zero, CursorMode.Auto);
            }
        }
        else if (currentState == CursorState.idleCantClick)
        {
            Cursor.SetCursor(cursorImages[2], Vector2.zero, CursorMode.Auto);
        }
        else if (currentState == CursorState.attack)
        {
            if (clicked)
            {
                Cursor.SetCursor(cursorImages[4], Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(cursorImages[3], Vector2.zero, CursorMode.Auto);
            }
        }
        else if (currentState == CursorState.loot)
        {
            if (clicked)
            {
                Cursor.SetCursor(cursorImages[6], Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(cursorImages[5], Vector2.zero, CursorMode.Auto);
            }
        }
        else if (currentState == CursorState.entrance)
        {
            if (clicked)
            {
                Cursor.SetCursor(cursorImages[8], Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(cursorImages[7], Vector2.zero, CursorMode.Auto);
            }
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            clicked = true;
        }
        else
        {
            clicked = false;
        }

    }

}
