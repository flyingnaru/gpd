﻿using UnityEngine;
using System.Collections;

public class rmk : MonoBehaviour {

    [SerializeField]
    GameObject EventSystem;
	// Use this for initialization
	IEnumerator Start () {

        EventSystem.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        EventSystem.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        EventSystem.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        EventSystem.SetActive(true);

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    
}
